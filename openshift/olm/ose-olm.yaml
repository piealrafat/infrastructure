---
- hosts: localhost
  gather_facts: false
  vars:
    openshift_host: "https://ose.{{ project_fqdn }}:8443"
    registry_namespace: olm
    artifact_server: docker.artifacts.{{ project_fqdn }}
    sdk_olm_registry_name: solute-catalog
    sdk_olm_registry_image: operators/operator-catalog:latest

  tags:
  - ose
  - operator
  - olm

  module_defaults:
    group/k8s:
      host: "{{ openshift_host }}"
      validate_certs: false

  tasks:
  - block:
    - name: Log in (obtain access token)
      k8s_auth:
        username: "{{ ose_admin_user }}"
        password: "{{ ose_admin_user_password }}"
      register: k8s_auth_results
      tags: sdk_creds

    - name: Assume no OLM, then prove wrong
      set_fact:
        olm_installed: "no"

    # This is messy, but currently there isn't a good way to determine OLM version
    - name: Determine if OLM is already installed (CSV CRD Exists)
      k8s_facts:
        api_key: "{{ k8s_auth_results.k8s_auth.api_key }}"
        version: apiextensions.k8s.io/v1beta1
        kind: CustomResourceDefinition
        name: clusterserviceversions.operators.coreos.com
      register: csv_crd_installed

    - name: Mark OLM Installed
      set_fact:
        olm_installed: "yes"
      when: csv_crd_installed.resources|length > 0

    - name: Determine if Correct version of olm is installed (OperatorGroup CRD Exists)
      k8s_facts:
        api_key: "{{ k8s_auth_results.k8s_auth.api_key }}"
        version: "apiextensions.k8s.io/v1beta1"
        kind: CustomResourceDefinition
        name: operatorgroups.operators.coreos.com
      register: og_crd_installed

    - name: Mark OLM as needing upgrade
      set_fact:
        olm_installed: "upgrade"
      when: 
        - csv_crd_installed.resources|length > 0
        - og_crd_installed.resources|length < 1

    # If old olm is installed, remove
    #- import_tasks: operator-lifecycle-manager-uninstall.yaml
    - name: Remove old OLM
      when: olm_installed == "upgrade"
      k8s:
        api_key: "{{ k8s_auth_results.k8s_auth.api_key }}"
        state: absent
        name: operator-lifecycle-manager
        kind: namespace

    - name: Install OLM 0.12.0
      import_tasks: olm-install.yaml
      when: olm_installed == "no" or olm_installed == "upgrade"
      vars:
        olm_version: "{{ sdk_olm_version }}"

    - name: Import the Operator Registry Catalog
      k8s:
        api_key: "{{ k8s_auth_results.k8s_auth.api_key }}"
        definition:
          apiVersion: operators.coreos.com/v1alpha1
          kind: CatalogSource
          metadata:
            name: "{{ sdk_olm_registry_name }}"
            namespace: olm
          spec:
            displayName: Solute Operator Registry
            image: "{{ sdk_olm_registry_image }}"
            publisher: SOLUTE, Inc.
            sourceType: grpc
      tags:
        - registry

    always:
    - name: If login succeeded, try to log out (revoke access token)
      when: k8s_auth_results.k8s_auth.api_key is defined
      k8s_auth:
        state: absent
        api_key: "{{ k8s_auth_results.k8s_auth.api_key }}"
      tags: sdk_creds
