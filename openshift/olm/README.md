# OLM

The Operator Lifecycle Manager (OLM) handles installation and upgrades of SDK Operators.

## Notes

* Openshift 3.11 default OLM version 0.6 is missing key functionality so these scripts install 0.12 for newer functionality
