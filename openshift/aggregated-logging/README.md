# Purpose

Provides Aggregated Logging feature for the entire cluster

# Tech Details

This implements an openshift-ansible playbook using the intended variables detailed here https://docs.openshift.com/container-platform/3.11/install_config/aggregate_logging.html#aggregate-logging-ansible-variables to install a fluentd DaemonSet to deliver all logs into an ElasticSearch cluster and provide a Kibana interface to review and parse the logs.

# Next Steps

Kibana can be found at kibana.apps.<clusted dns> by default. You can specify the hostname with variable `openshift_logging_kibana_hostname`.

# Performance

The ElasticSearch nodes need at least 16Gb of RAM. It is recommended to implement this on dedicated Infra nodes not combined with Master and to give them plenty of Ram. Also, provide as much drive space for storage as seems possible to account for a lot of information being gathered in these nodes.
