#!/bin/bash

if [ $# -ne 1 ]; then
    echo Usage: publish.sh siteadmin_password
    exit 1
fi

AGSADMIN=$ARCGIS_HOME/server/tools/admin
SITE=http://localhost:6080
LOCATION=/arcgis-storage/arcgis


$AGSADMIN/manageservice -u siteadmin -p $1 -s $SITE -n SampleWorldCities -o stop

$AGSADMIN/manageservice -u siteadmin -p $1 -s $SITE -n Utilities/Geometry -o start

$AGSADMIN/createservice -u siteadmin -p $1 -s $SITE -f $LOCATION/BlueMarble.sd

$AGSADMIN/createservice -u siteadmin -p $1 -s $SITE -f $LOCATION/Elevation.sd

$AGSADMIN/createservice -u siteadmin -p $1 -s $SITE -f $LOCATION/StreetMapUSA.sd
