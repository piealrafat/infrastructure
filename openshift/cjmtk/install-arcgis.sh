#!/bin/sh

if [ ! -e $CJ_HOME/CJSERV-10.5-LINUX.tgz  ]
then
	wget https://s3-us-gov-west-1.amazonaws.com/acs.arcgis/$CJSERV.tgz -P $CJ_HOME
	echo "Downloaded $CJSERV.tgz"
else 
	echo "$CJ_HOME/$CJSERV.tgz Already Exists. Extracting..."
fi



tar xfz $CJ_HOME/$CJSERV.tgz -C $CJ_HOME
echo "Extracted CJSERV"
rm $CJ_HOME/$CJSERV.tgz
echo "Cleaned up $CJSERV.tgz"

echo "-=-=-=-=-=- Calling CJSERV Setup -=-=-=-=-=-"


$CJ_HOME/$CJSERV/Software/Setup -m silent -v -l yes -a $CJ_HOME/$CJSERV/License/LicenseSvrAdv.ecp -d $CJ_HOME

echo "-=-=-=-=-=- CJSERV Setup Completed -=-=-=-=-=-"