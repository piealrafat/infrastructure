#!/bin/sh

echo "Starting the downloads!"
export ARCGIS_HOME="/arcgis-storage/arcgis"
export CJSERV="CJSERV-10.5-LINUX"

mkdir -p $ARCGIS_HOME || true

if [ ! -e $ARCGIS_HOME/server/startserver.sh ]
then
	echo "Copying ArcGIS Server Files"
	cp -r arcgis-install/arcgis/* $ARCGIS_HOME
else
	echo "Server File Already Exist. Not Copying."
fi


if [ ! -e $ARCGIS_HOME/BlueMarble.sd ]
then
	wget https://s3-us-gov-west-1.amazonaws.com/acs.arcgis/sd_files/BlueMarble.sd -P $ARCGIS_HOME
	echo "downloaded BlueMarble.sd"
else 
	echo "BlueMarble.sd already exists"
fi

if [ ! -e $ARCGIS_HOME/Elevation.sd ]
then
	wget https://s3-us-gov-west-1.amazonaws.com/acs.arcgis/sd_files/Elevation.sd -P $ARCGIS_HOME
	echo "downloaded Elevation.sd"
else 
	echo "Elevation.sd already exists"
fi

if [ ! -e $ARCGIS_HOME/StreetMapUSA.sd ]
then
	wget https://s3-us-gov-west-1.amazonaws.com/acs.arcgis/sd_files/StreetMapUSA.sd -P $ARCGIS_HOME
	echo "downloaded StreetMapUSA.sd"
else 
	echo "StreetMapUSA.sd already exists"
fi

if [ ! -e $ARCGIS_HOME/BlueMarble.zip ]
then
	wget https://s3-us-gov-west-1.amazonaws.com/acs.arcgis/zips/BlueMarble.zip -P $ARCGIS_HOME
	echo "downloaded BlueMarble.zip"
	unzip -q $ARCGIS_HOME/BlueMarble.zip -d $ARCGIS_HOME/gdb
	echo "unzipped BlueMarble.zip"
else 
	echo "BlueMarble.zip already exists. No need to download"
fi

#rm -rf $ARCGIS_HOME/BlueMarble.zip

if [ ! -e $ARCGIS_HOME/Elevation.zip ]
then
	wget https://s3-us-gov-west-1.amazonaws.com/acs.arcgis/zips/Elevation.zip -P $ARCGIS_HOME
	echo "downloaded Elevation.zip"
	unzip -q $ARCGIS_HOME/Elevation.zip -d $ARCGIS_HOME/gdb
	echo "unzipped Elevation.zip."
else 
	echo "Elevation.zip already exists. No need to download"
fi

#rm -rf $ARCGIS_HOME/Elevation.zip

if [ ! -e $ARCGIS_HOME/StreetMapUSA.zip ]
then
	wget https://s3-us-gov-west-1.amazonaws.com/acs.arcgis/zips/StreetMapUSA.zip -P $ARCGIS_HOME
	echo "downloaded StreetMapUSA.zip"
	unzip -q $ARCGIS_HOME/StreetMapUSA.zip -d $ARCGIS_HOME/gdb
	echo "Unzipped StreetMapUSA.zip"
else 
	echo "StreetMapUSA.zip already exists. No need to download"
fi

echo "Waiting for server to start..."
$ARCGIS_HOME/server/startserver.sh 

echo "Wait for Web Server to Start"
until $(curl --output /dev/null --silent --head --fail http://localhost:6080); do
    printf '.'
    sleep 5
done
echo "Web Server Started! Executing admin commands."


echo "Creating the new arcgis site!"
curl -d "username=siteadmin&password=siteadminPW" http://localhost:6080/arcgis/admin/createNewSite

echo "Publishing data to site!"
/publish.sh siteadminPW
echo "Finished publishing data - to view go to: http://localhost:6080/arcgis/manager"



# Cleanup
#rm -rf $ARCGIS_HOME/$CJSERV \
