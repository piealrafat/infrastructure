---
# Depends on OSE Admin User created by ose_ldap playbook.
- hosts: localhost
  gather_facts: false
  vars:
    operator_namespace: sdk-gitea
    operator_namespace_display_name: "SDK Gitea"
    operator_namespace_description: "Namespace dedicated to the running and maintenance of the Gitea operator."
    openshift_host: "https://{{ openshift_master_cluster_hostname }}:8443"
    app_domain: "apps.{{ project_fqdn }}"
    project: Operators
    project_plural: operators
    description: "Namespace dedicated to the running and maintenance of the Gitea operator."
    artifact_server: "{{ artifactory_docker_url }}"
    template_dir: "/app/ansible/playbooks/templates"
    postgres_operator_config_name: "postgresql-operator-default-configuration"

  tags:
  - ose
  - operator
  - namespace

  module_defaults:
    group/k8s:
      host: "{{ openshift_host }}"
      validate_certs: false

  tasks:
  - block:
    - name: Log in (obtain OpenShift access token)
      k8s_auth:
        username: "{{ ose_admin_user }}"
        password: "{{ ose_admin_user_password }}"
      register: k8s_auth_results_operators
      tags: operator-login

    - import_tasks: subscriptions.yaml
      tags: gitea

    - name: Create {{ operator_namespace }} namespace
      k8s:
        api_key: "{{ k8s_auth_results_operators.k8s_auth.api_key }}"
        definition:
          api_version: v1
          kind: Project
          metadata:
            name: "{{ operator_namespace }}"
            annotations:
              "openshift.io/display-name": "{{ operator_namespace_display_name }}"
              "openshift.io/description": "{{ operator_namespace_description }}" 
          state: present
      changed_when: 
      - stdout is defined
      - ('Now using project' in stdout)      
      tags: gitea

    - name: Create Jaeger Service
      k8s:
        api_key: "{{ k8s_auth_results_operators.k8s_auth.api_key }}"
        definition:
          apiVersion: v1
          kind: Gitea
          metadata:
            name: gitea
            namespace: "{{ operator_namespace }}"
          spec:
            size: 3 
      tags: gitea

    always:
    - name: If login succeeded, try to log out (revoke access token)
      when: k8s_auth_results_operators.k8s_auth.api_key is defined
      k8s_auth:
        state: absent
        api_key: "{{ k8s_auth_results_operators.k8s_auth.api_key }}"
      tags: operator-login
