# SDK Service Namespace

This install script handles setting up gitea in the services namespace. 

The operator is described in the "subscriptions.yaml", but set to when: false because they are either not yet in use or are namespaced operators so only provided as an example (strimzi kafka).