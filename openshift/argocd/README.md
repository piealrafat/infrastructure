# ArgoCD

ArgoCD is a GitOps tool for synchronizing K8s/OCP yaml descriptor files from a Git repository into a cluster. ArgoCD is used for end to end deployment pipelines as well as general cluster configuration.

## Notes

* Argo requires permissions against the cluster and so should be deployed with caution
* Argo is a relatively new tool in the SDK and is considered Tech Preview status
