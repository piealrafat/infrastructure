# Tiller

The Tiller service is the cluster component for Helm installs.

## Notes

* Tiller should be considered an optional component in favor of Operator based installs