# Keel

Keel is an operator that watches running containers and updates based off configured rules as labels/annotations on pods and deployments.

## Notes

* This install should be migrated to OLM catalog based install
* Keel user/pass should be set dynamically through an encrypted vault
* Keel requires admin access to runtime environment so should be used with appropriate permissions as necessary