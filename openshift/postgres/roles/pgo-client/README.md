Role Name
=========

A role to install Postgres Operator (pgo) Client

Requirements
------------

* Server must have Postgres Operator (pgo) installed

Role Variables
--------------

|variable|type|default|description|
|--------|----|-------|-----------|
|pgo_admin_username|string|admin|Admin username set for pgo client, overrided by vault|
|pgo_admin_password|string|password|Admin password set for pgo client, overrided by vault|
|pgo_owner|string|n/a|Default owner of the .pgo directory for storing pgouser file|
|pgo_group|string|n/a|Default group of the .pgo directory for storing pgouser file|
|pgo_homedir|string|/usr/local/bin|Default bin location for installing the pgo client binary|

Dependencies
------------

N/A

Example Playbook
----------------

```yaml
- hosts: localhost
  roles:
  - pgo-client
```

License
-------

Apache 2.0

Author Information
------------------

Solute
