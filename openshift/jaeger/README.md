# Jaeger

Jaeger is a distributed tracing platform used for monitoring microservices-based distributed systems.

This install script handles setting up jaeger in the sdk-jeager namespace. 

The operator is described in the "subscriptions.yaml", but set to when: false because they are either not yet in use or are namespaced operators so only provided as an example (strimzi kafka).