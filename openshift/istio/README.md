# Istio

Istio is a service mesh component for K8s and Openshift environments.

## Notes

* This install is considered alpha and tech preview. Use with caution
* The install yaml is built from the demo profile with some Openshift customizations
* The install yaml is entirely static with no customizations
* The Istio operator on Openshift does not appear to work with OLM 0.12 but should be considered for future installs