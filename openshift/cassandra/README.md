# DEPRECATED - Cassandra Operator

The Cassandra Operator is used to install Cassandra clusters and data centers inside the SDK environment. This operator is now managed through the OLM repository and will be removed in future releases.

