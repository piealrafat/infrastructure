# DEPRECATED - Anchore

This helm chart installs Anchore Community Engine in the SDK environment. Anchore is a tool to scan Docker images and is used in the CI/CD pipelines for generating container artifacts.

## Notes

* This install is deprecated in favor of the OLM operator based installation