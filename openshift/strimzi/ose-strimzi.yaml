---
# Depends on OSE Admin User created by ose_ldap playbook.
- hosts: localhost
  gather_facts: false
  vars:
    operator_namespace: sdk-strimzi
    operator_namespace_display_name: "SDK Strimzi"
    operator_namespace_description: "Namespace dedicated to the running and maintenance of the Strimzi operator."
    operator_tag: "strimzi"
    openshift_host: "https://{{ openshift_master_cluster_hostname }}:8443"
    app_domain: "apps.{{ project_fqdn }}"
    project: Operators
    project_plural: operators
    description: "Namespace dedicated to the running and maintenance of the Strimzi operator."
    template_dir: "/app/ansible/playbooks/templates"
    postgres_operator_config_name: "postgresql-operator-default-configuration"

  tags:
  - ose
  - operator
  - namespace

  module_defaults:
    group/k8s:
      host: "{{ openshift_host }}"
      validate_certs: false

  tasks:
  - block:
    - name: Log in (obtain OpenShift access token)
      k8s_auth:
        username: "{{ ose_admin_user }}"
        password: "{{ ose_admin_user_password }}"
      register: k8s_auth_results_operators
      tags: operator-login

    - name: Create Strimzi Application
      k8s:
        api_key: "{{ k8s_auth_results_operators.k8s_auth.api_key }}"
        namespace: services
        definition:
          apiVersion: v1alpha1
          kind: Application
          metadata:
            name: "{{ operator_tag }}"
          spec:
            destination:
              namespace: "{{ operator_namespace }}"
              server: 'https://kubernetes.default.svc'
            project: sdk-automation
            source:
              directory:
                jsonnet: {}
                recurse: true
              path: ./strimzi/
              repoURL: '{{ sdk_services_repo_url }}'
              targetRevision: "{{ sdk_services_repo_ref | default('master') }}"
            syncPolicy:
              automated: {}
      tags: "{{ operator_tag }}"

    always:
    - name: If login succeeded, try to log out (revoke access token)
      when: k8s_auth_results_operators.k8s_auth.api_key is defined
      k8s_auth:
        state: absent
        api_key: "{{ k8s_auth_results_operators.k8s_auth.api_key }}"
      tags: operator-login
