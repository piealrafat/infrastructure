# SDK Operators Namespace

This install script handles setting up key SDK operators for environment services

Some operators are described in the "subscriptions.yaml", but set to when: false because they are either not yet in use or are namespaced operators so only provided as an example (strimzi kafka).