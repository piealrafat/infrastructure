# AWX

AWX is the upstream open source version of Ansible Tower. It is used to centralize the management and configuration of Ansible playbooks and roles. This ansible playbook will install AWX inside the SDK cluster.

## Notes

* AWX is not currently installed or tested inside SDK so the playbook may be old or out of date. Use at your own risk.