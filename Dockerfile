FROM ubuntu:18.04
LABEL maintainer=metheney.josh@solute.us
VOLUME /app
WORKDIR /app

# Ansible will not use ansible.cfg in a world writeable directory
RUN ["chmod", "o-w", "/app"]

# entrypoint to the container. really just passes off to the cli tool
ADD build/entrypoint.sh /entrypoint.sh
RUN ["chmod", "+x", "/entrypoint.sh"]
ENTRYPOINT ["/entrypoint.sh"]

# install some apt dependencies and clean up after ourselves
RUN apt-get -q update && \
    apt-get -q -y install python-pip unzip wget openssh-client curl default-jre-headless && \
    apt-get -q clean

# install some python dependencies including ansible
ADD build/requirements.txt /tmp/requirements.txt
RUN pip install -q -r /tmp/requirements.txt

# install terraform
ARG TERRAFORM_VERSION=0.12.29
RUN wget --quiet -O /tmp/tf.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip -q /tmp/tf.zip -d /usr/bin/ && \
    rm -f /tmp/tf.zip

# install ansible provider for terraform
ARG ANSIBLE_PROVIDER_VERSION=v1.0.3
RUN wget --quiet -O /tmp/tfap.zip https://github.com/nbering/terraform-provider-ansible/releases/download/${ANSIBLE_PROVIDER_VERSION}/terraform-provider-ansible-linux_amd64.zip && \
    unzip -q /tmp/tfap.zip -d /tmp && \
    mv /tmp/linux_amd64/terraform-provider-ansible_${ANSIBLE_PROVIDER_VERSION} /usr/bin/ && \
    rm /tmp/tfap.zip

# We're using self-signed certificates
ENV K8S_AUTH_VERIFY_SSL=no

ADD . /app/
