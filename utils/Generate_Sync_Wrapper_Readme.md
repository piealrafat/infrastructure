## Operator Sync Create Script Readme

### Pre-Reqs:
Openshift cli access
Credentials to openshift cluster : *oc login*

### Create the Wrapper
Using the example *create-sync-example-wrapper.sh*
we are able to generate a 'Sync' type yaml for openshift to ingest for our sync-operator to utilize. With the resulting yaml, the sync-operator is succesfully able to monitor our specified images for changes and push these changes and sync images into the IL5 s3 bucket.



* When launching the script you can define the image version ie:

```
sh image_name.sh v1
```
This will define what image version you want the sync-operator to watch changes for. The *latest* version comes as default, so you can simply run the script as:

```
sh image_name.sh
```
* Create your own image_name.sh script using the template described above. Ensure to change these variables to match the images and artifactory folder of the image in reference.

 ```bash
 images="track-service-operator track-service-ui track-service track-service-stream track-service-sim track-service-geo"

 tag=${1:-latest}
 repo="docker.artifacts.cms.solute.us"       (1)
 repofolder="track-service"                  (2)
 bucket="peoiws1-devsecops-s3-bucket/sync"   (3)
 createsync="./create-sync.sh"               (4)
 ```
1. Docker artifactory url
2. Docker folder image resides in
3. Destination s3 bucket
4. sync script path.  (be sure to run this image_name.sh in the same directory as this one)

The conclusion of this script takes all previously defined vars and generates the command used to call the *create-sync*.sh script. This generates the yaml document of type sync and then using openshift cli command pushes it up to the defined name space found in the cluster.
 ```bash
 for i in $images; do
  ./create-sync.sh ${i}-${tag} ${repo}/${repofolder}/${i}:${tag} s3://${bucket}/${i}-${tag}.tar | oc create -f -
 done
 ```
* Note to change *oc create -f* at the end of the command to 'oc replace -f' when updating the resulting document with yamll ready existing sync yamls

### Sync Script Elaborated

This script implements defined secrets and desired namespace, and then compiles our yaml doc based on input and vars used to launch the script.

* Dependent on the final lines of the wrapper, the script calls for a type of source to define within the generated yaml doc.

```bash
if [[ $SRC == "s3://"* ]]; then
	# Is s3 object
	path=${SRC#*://*/}
	bucket=$(echo ${SRC#*://} |cut -d/ -f1)
	DOCUMENT+=$(cat << HEREDOC
  source:
    bucket: $bucket
    path: $path
    region: $REGION
    secret: $SRCSECRET
    type: s3
```    


* The same will define the type of destination to call for and location path of the image to sync to.

```bash
if [[ $DST == "s3://"* ]]; then
	# Is s3 object
	path=${DST#*://*/}
	bucket=$(echo ${DST#*://} |cut -d/ -f1)
	DOCUMENT+=$(cat << HEREDOC

  destination:
    bucket: $bucket
    path: $path
    region: $REGION
    secret: $DSTSECRET
    type: s3
HEREDOC
```

### Verify
Search via *oc get* or visit the openshift cluster Cluster Console, and search for all things of type sync. Your newly generated or updated sync yaml should exist in the defined namespace.
