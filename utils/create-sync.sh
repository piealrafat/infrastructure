#!/bin/bash

NAME=${1//_/-}
NAMESPACE="track-service-ops"
SRC=$2
DST=$3

SRCSECRET="docker-creds"
DSTSECRET="aws-il5"
REGION="us-gov-west-1"

usage() {
echo "Usage: $0 sync-name source destination

  S3 connections should use the s3://bucket/object syntax.
  Docker images should include the tag and be in the format registry/folder/name:tag.

	HINT: Use with oc create in this manner:
	\`$0 sync-name source destination | oc create -f -\`
"
exit
}

[[ $# -ne 3 ]] && usage

# Initiate sync document
DOCUMENT=$(cat << HEREDOC
apiVersion: cms.solute.us/v1beta1
kind: Sync
metadata:
  name: $NAME
  namespace: $NAMESPACE
spec:

HEREDOC
)

# Add source
# TODO: Figure out a better way to identify a docker string
if [[ $SRC == "s3://"* ]]; then
	# Is s3 object
	path=${SRC#*://*/}
	bucket=$(echo ${SRC#*://} |cut -d/ -f1)
	DOCUMENT+=$(cat << HEREDOC

  source:
    bucket: $bucket
    path: $path
    region: $REGION
    secret: $SRCSECRET
    type: s3

HEREDOC
  )
elif [[ $SRC == *"@"* ]]; then
	# Is container image
	DOCUMENT+=$(cat << HEREDOC

  source:
    image: $SRC
    secret: $SRCSECRET
    type: docker
HEREDOC
  )
elif [[ $SRC == *":"* ]]; then
	# Is container image
	DOCUMENT+=$(cat << HEREDOC

  source:
    image: $SRC
    secret: $SRCSECRET
    type: docker
HEREDOC
  )
else
	echo "Source path is not a recognized format. Must be a image in a docker registry with tag or a properly formatted S3 link."
	exit
fi

# Add destination
# TODO: Figure out a better way to identify a docker string
if [[ $DST == "s3://"* ]]; then
	# Is s3 object
	path=${DST#*://*/}
	bucket=$(echo ${DST#*://} |cut -d/ -f1)
	DOCUMENT+=$(cat << HEREDOC

  destination:
    bucket: $bucket
    path: $path
    region: $REGION
    secret: $DSTSECRET
    type: s3

HEREDOC
  )
elif [[ $DST == *"@"* ]]; then
	# Is container image
	DOCUMENT+=$(cat << HEREDOC

  destination:
    image: $DST
    secret: $DSTSECRET
    type: docker
HEREDOC
  )
elif [[ $DST == *":"* ]]; then
	# Is container image
	DOCUMENT+=$(cat << HEREDOC

  destination:
    image: $DST
    secret: $DSTSECRET
    type: docker
HEREDOC
  )
else
	echo "Destination path is not a recognized format. Must be a image in a docker registry with tag or a properly formatted S3 link."
	exit
fi

echo "$DOCUMENT"
