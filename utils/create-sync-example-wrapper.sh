#!/bin/bash
# Example wrapper script that depends on the ./create-sync.sh in the same directory for its logic.

# Add a docker image here to have it included in the list of synced images
images="track-service-operator track-service-ui track-service track-service-stream track-service-sim track-service-geo"

default_repofolder="operators"
default_image="sync-operator"
default_tag=${1:-stable}
default_bucket="mybucket/folder"
default_repofolder="track-service"
default_repo="docker.artifacts.cms.solute.us"

usage() {
  echo "$0 tag"
  echo "  tag is an optional field. Default is latest."
}

create_sync() {
  repofolder=${1:-default_repofolder}
  image=${2:-default_image}
  tag=${3:-default_tag}
  bucket=${default_bucket}
  repo=${default_repo}

  ./create-sync.sh ${image}-${tag} ${repo}/${repofolder}/${image}:${tag} s3://${bucket}/${image}-${tag}.tar | oc replace -f -
}

if [[ $1 -eq "--help" ]]; then usage; fi

for i in $images; do
  tag="latest"
  create_sync $default_repofolder $i $tag
done
