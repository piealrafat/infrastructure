#!/bin/bash
## Script: sdk-cli.sh
## Author: Josh Metheney <metheney.josh@solute.us>
## Author: Brock Renninger <renninger.brock@solute.us>
## Company: SOLUTE
## License: Apache 2.0
## Description: Top level script to manage the SDK DevSecOps Environment

VERSION=0.1
APPDIR="/app/"
SKIP_CHECKS=false
BACKEND="aws"
CLI="${0}"

version() {
  echo "Version ${VERSION}"
}

usage() {
  echo ""
  echo "Usage: ${0} [options...] COMMAND [command options...]"
  echo "OPTIONS"
  echo " -h  display usage and help text"
  echo " -v  display script version number"
  echo " -d  set the application directory. defaults to /app/"
  echo " -b  set the IaaS backend. Must be a subdir under terraform/. Defaults to aws"
	echo " -s  skip all checks. Helpful when not using the provisioning capability"
  echo ""
  echo "COMMANDS"
  echo " init       Run any necessary initialization steps"
  echo " provision  Provision the infrastructure and base nodes"
  echo " destroy    Tear down the infrastructure and base nodes"
  echo " install    Run installation scripts to configure base nodes"
  echo " deploy     Deploy a component found in the openshift/ directory"
  echo ""
}

set_backend() {
  if [[ ! -d terraform/${1} ]]; then
    echo "ERROR: Backend ${1} does not exist"
    exit 1
  fi
  BACKEND="${1}"
}

while getopts ":hvsd:b:" opt; do
  case ${opt} in
    h )
      usage
      exit 0
      ;;
    v )
      version
      exit 0
      ;;
    d )
      APPDIR="${OPTARG}"
      ;;
    s )
      SKIP_CHECKS=true
      ;;
    b )
      set_backend ${OPTARG}
      ;;
    \? )
      echo "Invalid option: ${OPTARG}" 1>&2
      usage
      exit 1
      ;;
    : )
      echo "Invalid option: ${OPTARG} requires an argument" 1>&2
      usage
      exit 1
      ;;
  esac
done
shift $((OPTIND -1))

checks() {
  # Exit if the application dir does not exist
  ## TODO: Is there a better way to have an application directory?
  if [[ ! -d ${APPDIR} ]]; then
    echo "ERROR: Application Dir ${APPDIR} does not exist"
    exit 1
  fi



  # Check if Terraform exists and is the appropriate version
  ## TODO: create a more robust version check
  TERRAFORM="$( which terraform )"
  if [[ "${TERRAFORM}" == "" ]]; then
    echo "ERROR: terraform binary not found in path"
    exit 1
  fi

  ${TERRAFORM} --version | grep ^Terraform | grep -q 0.12
  if [[ $? != 0 ]]; then
    echo "WARNING: terraform version has not been tested"
  fi

  # Check if Ansible exists and is the appropriate version
  ## TODO: create a more robust version check
  ANSIBLE_PLAYBOOK="$( which ansible-playbook )"
  if [[ "${ANSIBLE_PLAYBOOK}" == "" ]]; then
    echo "ERROR: ansible-playbook binary not found in path"
    exit 1
  fi

  ${ANSIBLE_PLAYBOOK} --version | grep ^ansible-playbook | grep -q 2.6
  if [[ $? != 0 ]]; then
    echo "WARNING: ansible-playbook version has not been tested"
  fi

  # Check if AWS creds are set

  if [[ "${BACKEND}" == "aws" ]]; then
    if [[ -z "${AWS_DEFAULT_REGION}" ]]; then
      echo "ERROR: Must set the AWS_DEFAULT_REGION environment variable"
      exit 1
    fi
    if [[ "${SKIP_CHECKS}" == "false" ]]; then
      if [[ $(curl -sL --connect-timeout 12 -w "%{http_code}\n" http://169.254.169.254/latest/meta-data/iam/security-credentials/ -o /dev/null) != 200 ]]; then
        if [[ -z "${AWS_ACCESS_KEY_ID}" ]]; then
          echo "ERROR: Must set the AWS_ACCESS_KEY_ID environment variable"
          exit 1
        fi
        if [[ -z "${AWS_SECRET_ACCESS_KEY}" ]]; then
          echo "ERROR: Must set the AWS_SECRET_ACCESS_KEY environment variable"
          exit 1
        fi
      fi
    fi
  fi

  # Check if backend exists
  if [[ ! -d terraform/${BACKEND} ]]; then
    echo "ERROR: Backend configuration ${BACKEND} does not exist"
    exit 1
  fi
}


## TODO: Eventually look at moving functions to separate script files to keep things clean
init() {
  if [[ ! -d keys ]]; then
    echo "Make keys dir"
    mkdir -p keys
  fi

  if [[ ! -f keys/id_rsa ]]; then
    echo "Make keys"
    ssh-keygen -b 2048 -t rsa -f keys/id_rsa -q -N ""
  fi

  if [[ -d terraform/${BACKEND}/init ]]; then
    echo "Terraform remote state"
    ${TERRAFORM} init terraform/${BACKEND}/init/
    ${TERRAFORM} apply -auto-approve terraform/${BACKEND}/init/
  fi

  (
    cd terraform/${BACKEND}
    ${TERRAFORM} init
  )
}

provision_usage() {
  echo ""
  echo "USAGE: ${CLI} [options...] apply [-y]"
  echo ""
  echo "-y  append auto-approve to the terraform apply command"
  echo ""
}

provision() {
  APPROVE=""
  while getopts ":hy" opt; do
    case ${opt} in
      h )
        provision_usage
        exit 0
        ;;
      y )
        APPROVE="-auto-approve"
        ;;
      \? )
        echo "Invalid option: ${OPTARG}" 1>&2
        usage
        exit 1
        ;;
      : )
        echo "Invalid option: ${OPTARG} requires an argument" 1>&2
        usage
        exit 1
        ;;
    esac
  done
  shift $((OPTIND -1))

  (
    cd terraform/${BACKEND}
	${TERRAFORM} init "-lock=false"
    ${TERRAFORM} apply ${APPROVE}
  )
}

destroy_usage() {
  echo ""
  echo "USAGE: ${CLI} [options...] destroy [-y]"
  echo ""
  echo "-y  append auto-approve to the terraform destroy command"
  echo ""
}

destroy() {
  setup_ssh

  APPROVE=""
  while getopts ":hy" opt; do
    case ${opt} in
      h )
        destroy_usage
        exit 0
        ;;
      y )
        APPROVE="-auto-approve"
        ;;
      \? )
        echo "Invalid option: ${OPTARG}" 1>&2
        usage
        exit 1
        ;;
      : )
        echo "Invalid option: ${OPTARG} requires an argument" 1>&2
        usage
        exit 1
        ;;
    esac
  done
  shift $((OPTIND -1))

  ANSIBLE_TF_DIR=terraform/${BACKEND} ${ANSIBLE_PLAYBOOK} ansible/playbooks/cleanup.yaml
  (
    cd terraform/${BACKEND}
    ${TERRAFORM} destroy ${APPROVE}
  )
}

deploy_usage() {
  echo ""
  echo "USAGE: ${CLI} [options...] deploy COMPONENT"
  echo ""
  echo "COMPONENT  name of a component to install. Should be the name of a sub-dir under openshift/"
  echo ""
}

deploy() {
  local component=$1
  if [[ ! -d openshift/${component} ]]; then
    echo "ERROR: Component ${component} does not exist"
    exit 1
  fi
  if [[ ! -f openshift/${component}/ose-${component}.yaml ]]; then
    echo "ERROR: Component does not seem to have an installation playbook"
    exit 1
  fi

  setup_ssh
  ANSIBLE_TF_DIR=terraform/${BACKEND} ${ANSIBLE_PLAYBOOK} openshift/${component}/ose-${component}.yaml
}

install() {
  setup_ssh
  chmod u+x ${APPDIR}/ansible/inventory/terraform.py
  ANSIBLE_TF_DIR=terraform/${BACKEND} ${ANSIBLE_PLAYBOOK} ansible/playbooks/install.yaml
}

setup_ssh() {
  #TODO: Find better way to handle ssh keys.
  ## Currently using ssh-agent because my docker windows mount doesn't let me set
  ## the ssh id_rsa key file to 0600 permissions. Load it into ssh-agent so ansible works
  if [[ -z "${SSH_AUTH_SOCK}" ]]; then
    eval `ssh-agent`
    if [[ -f keys/id_rsa ]]; then
      cat keys/id_rsa | ssh-add -k -
    fi
  fi
  if [[ $(grep "X.X.X.X" /root/.ssh/config | wc -l ) == 1 ]]; then
    echo "Adding bastion IP to ssh config for root user"
    cd terraform/${BACKEND}
    ${TERRAFORM} apply
    BASTION_IP=$(${TERRAFORM} output bastion_ip_addr)
    sed -i "s/X.X.X.X/${BASTION_IP}/g" /root/.ssh/config
    cd /app
  fi
}

bash() {
  setup_ssh
  ANSIBLE_TF_DIR=/app/terraform/${BACKEND} /bin/bash
}

main() {
  subcommand=${1}
  shift
  if [[ "${subcommand}" == "" ]]; then
    echo "Must specify a command to run"
    usage
    exit 1
  fi

  checks

  # cd to the application dir and parse the command
  ## This case statement should only run functions and not do anything fancy inline
  cd ${APPDIR}
  case "${subcommand}" in
    init)
      echo "Initializing..."
      init
      ;;
    provision)
      echo "Provisioning..."
      provision $@
      ;;
    destroy)
      echo "Destroying..."
      destroy $@
      ;;
    install)
      echo "Installing..."
      install
      ;;
    deploy)
      echo "Deploying..."
      deploy $@
      ;;
    bash)
      bash
      ;;
    *)
      echo "Invalid Command: ${subcommand}"
      usage
      exit 1
      ;;
  esac
}
main $@
