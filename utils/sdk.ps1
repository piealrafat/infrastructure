function Run-CMSContainer {
  docker run --rm -it `
  -v ${PWD}:/app `
  -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} `
  -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} `
  -e AWS_DEFAULT_REGION="us-gov-east-1" `
  -e VAULT_PASSWORD=${VAULT_PASSWORD} `
  -e ANSIBLE_CONFIG=/app/ansible.cfg `
  sdk-builder:latest
}

If (-NOT (Test-Path variable:global:VAULT_PASSWORD)) {
  $value = Read-Host -AsSecureString -Prompt 'What is the vault password?'
  New-Variable -Name VAULT_PASSWORD -Visibility 'Private' -Value $value -Scope 'Global'
  #Get-Variable -Name VAULT_PASSWORD
}

If ( (Test-Path 'variable:global:AWS_ACCESS_KEY_ID','variable:global:AWS_SECRET_ACCESS_KEY') -contains $false ) {
  $value1 = Read-Host -AsSecureString -Prompt 'What is the AWS Access Key ID?'
  $value2 = Read-Host -AsSecureString -Prompt 'What is the AWS Secret Access Key?'
  New-Variable -Name AWS_ACCESS_KEY_ID -Visibility 'Private' -Value $value1 -Scope 'Global'
  New-Variable -Name AWS_SECRET_ACCESS_KEY -Visibility 'Private' -Value $value2 -Scope 'Global'
}

Set-Alias sdk Run-CMSContainer
