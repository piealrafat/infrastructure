#!/bin/bash

APPDIR=/app

if [[ ! -f ${APPDIR}/utils/sdk-cli.sh ]]; then
  echo "ERROR: ${APPDIR}/utils/sdk-cli.sh does not exist"
  echo "  Have you mounted the application directory to the /app volume?"
  exit 1
fi

${APPDIR}/utils/sdk-cli.sh $@
