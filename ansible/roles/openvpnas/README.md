OpenVPN AS
=========

Ansible role to install Openvpn Access Server on a RHEL 7.x node.

Requirements
------------

Must be run against a RHEL 7.x host with internet connectivity for the OpenVPN AS RPM.

Role Variables
--------------

|variable|type|default|description|
|--------|----|-------|-----------|
|openvpnas_password|string|n/a|if set the role will configure the default user password on the system|
|openvpnas_user|string|openvpn|default user for login.|
|openvpnas_rpm|string|https://openvpn.net/downloads/openvpn-as-latest-CentOS7.x86_64.rpm|default url for openvpn as rpm|
|openvpnas_reroute_gw|boolean|False|sets whether or not all client traffic will route through the vpn|
|openvpnas_public_ip|string|n/a|if set the role will configure the public hostname. useful if in a nat environment|
|openvpnas_private_net|string|n/a|if set the role will configure allowed subnets for client access|
|openvpnas_ldap_auth|boolean|n/a|if set to true the rol will configure ldap auth|
|openvpnas_user_group|string|n/a|ldap user group to allow logins|
|rh_idm_groupou|string|n/a|ldap ou for group objects|
|openvpnas_bind_user|string|n/a|ldap bind user for queries|
|openvpnas_bind_user_password|string|n/a|ldap bind user password for queries|
|rh_idm_fqdn|string|n/a|fqdn of the ldap server|
|rh_idm_userou|string|n/a|ldap ou for user objects|


Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: vpn-servers
      gather_facts: True
      roles:
         - { role: openvpnas }

License
-------

Apache 2.0

Author Information
------------------

Josh Metheney <metheney.josh@solute.us>
