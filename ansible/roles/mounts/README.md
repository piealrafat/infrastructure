# Ansible Role: Mounts

Ansible role to configure additional entries in `/etc/fstab`.

Tests for filesystem on a defined partition. If none, partitions and mounts to the defined directory.
Note that the directory currently only gets created with root ownership with 0755 (root; read write execute).

## Requirements

* Only runs on systems with `/etc/fstab` (e.g. Linux, Solaris, etc)
* Requires the proper filesystem type libraries be installed on the system (e.g. xfs)

## Role Variables

|variable |type                 |default |description                                                               |
|---------|---------------------|--------|--------------------------------------------------------------------------|
|mounts   |list of dictionaries |empty   |if set the role will loop over each item and configure the defined mounts |

### `mounts` variable

The mounts variable is a list of dictionaries. Each item in the list should be a dictionary with the following key/value pairs:

|key   |type  |required|description                                              |
|------|------|--------|---------------------------------------------------------|
|dev   |string|yes     |full path to the device that is to be mounted            |
|fstype|string|yes     |filesystem type to mount the device                      |
|dir   |string|yes     |full path to the directory the filesystem will be mounted|

example mounts var definition:
```yaml
mounts:
  - { dev: "/dev/sda1", fstype: "xfs", dir: "/data" }
  - { dev: "/dev/sdb1", fstype: "ext4", dir: "/data1" }
```

## Dependencies

None.

## Example Playbook

```yaml
- hosts: all
  vars:
    mounts:
      - { dev: "/dev/nvme1n1", fstype: "xfs", dir: "/data" }
  roles:
    - mounts
```

## License

Apache 2.0

## Author Information

Solute
