# Ansible Role: Artifactory

Ansible role to configure the artifactory docker container.

## Requirements

Docker 1.13 or later

## Role Variables
|variable|type|default|description|
|--------|----|-------|-----------|
|docker_compose_binary|string|/usr/local/bin/docker-compose|Directory location of docker compose binary|
|artifactory_clean|boolean|true|When set to true this will create the artifactory repos|
|artifactory_import|boolean|false|When set to true this will import the artifactory repos|
|artifactory_client_certs|string|n/a|PEM file location for the artifactory client certs|
|artifactory_base_dir|string|/data|Location for the artifactory base directory|
|artifactory_create_ssl|boolean|true|When set to true this will create the artifactory ssl|
|artifactory_version|string|6.11.1|Version of artifactory to be deployed|
|artifactory_container_base|string|docker.bintray.io/jfrog|Name of the artifactory container base|
|artifactory_container|string|artifactory-pro|Name of the artifactory container|
|artifactory_nginx_container|string|nginx-artifactory-pro|Name of the nginx artifactory container|
|artifactory_nginx_owner|integer|104|ID of the owner assigned to artifactory nginx|
|artifactory_nginx_group|integer|104|ID of the group assigned to artifactory nginx|
|artifactory_owner|integer|1030|ID of the owner assigned to artifactory|
|artifactory_group|integer|1030|ID of the group assigned to artifactory|
|artifactory_default_password|string|password|Default password assigned to artifactory|
|artifactory_dirs|list|n/a|If set the role will loop through each directory item and apply the given directory settings|
|artifactory_repos|list|n/a|List of artifactory repos identified by a key|
|artifactory_license|string|{{ vault_artifactory_license }}|License assigned to artifactory|
|artifactory_username|string|{{ vault_artifactory_username }}|Username assigned to artifactory|
|artifactory_password|string|{{ vault_artifactory_password }}|Password assigned to artifactory|
|artifactory_url|string|n/a|API endpoint for the artifactory URL|

## Dependencies

None.

## Example Playbook

```yaml
- hosts: artifactory_servers
  roles:
    - docker
    - artifactory
```

## License

Apache 2.0

## Author Information

Solute
