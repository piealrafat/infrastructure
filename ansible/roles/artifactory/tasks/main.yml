---

- name: "create directory"
  file:
    path: "{{ item.path }}"
    state: "directory"
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
    mode: "{{ item.mode }}"
  loop: "{{ artifactory_dirs }}"

- name: "Create docker compose"
  template:
    src: "artifactory.yaml.j2"
    dest: "{{ artifactory_base_dir }}/docker-compose.yaml"
  register: template_compose

- name: "Install packages for certs [pyOpenSSL]"
  yum:
    name: "python2-cryptography"

# TODO: Maybe check if these certs need regenerated (expired)
- name: "Create nginx key"
  openssl_privatekey:
    path: "{{ artifactory_base_dir }}/nginx/ssl/example.key"
    owner: "{{ artifactory_nginx_owner }}"
    group: "{{ artifactory_nginx_group }}"

- name: "Create nginx csr"
  openssl_csr:
    common_name: "{{ ansible_fqdn }}"
    subject_alt_name: "DNS:*.{{ ansible_fqdn }},DNS:{{ ansible_fqdn }}"
    country_name: "US"
    organization_name: "CMS"
    privatekey_path: "{{ artifactory_base_dir }}/nginx/ssl/example.key"
    path: "{{ artifactory_base_dir }}/example.csr"
    owner: "{{ artifactory_nginx_owner }}"
    group: "{{ artifactory_nginx_group }}"

- name: "Create nginx certs"
  openssl_certificate:
    path: "{{ artifactory_base_dir }}/nginx/ssl/example.pem"
    privatekey_path: "{{ artifactory_base_dir }}/nginx/ssl/example.key"
    csr_path: "{{ artifactory_base_dir }}/example.csr"
    provider: selfsigned
    owner: "{{ artifactory_nginx_owner }}"
    group: "{{ artifactory_nginx_group }}"

# Add the license file to the volume mount.
#  Artifactory-pro container should take care of installing
- name: "Artifactory Pro License"
  include_tasks: license.yml
  when: artifactory_license is defined

# add entitlement info and ca certs if
#  configuring upstream red hat repos.
#  assumes entitlements are already set up!
- name: "Artifactory RHEL Setup"
  include_tasks: rhel.yml
  when:
    - ansible_os_family == 'RedHat'
    - rh_byol|bool

# Use this for importing existing repos
#  Useful when installing the environment offline and using
#  cached repos that are packaged up.
# TODO: clearly document this workflow and how it operates
- name: "Import any repos"
  include_tasks: import.yml
  when: artifactory_import|bool

- name: "Check state of compose"
  command: "{{ docker_compose_binary }} -f {{ artifactory_base_dir }}/docker-compose.yaml -p artifactory ps -q"
  register: ps_compose
  changed_when: False

- name: "Start the services"
  command: "{{ docker_compose_binary }} -f {{ artifactory_base_dir }}/docker-compose.yaml -p artifactory up -d"
  when: ps_compose.stdout == ""

- name: "Restart the services"
  command: "{{ docker_compose_binary }} -f {{ artifactory_base_dir }}/docker-compose.yaml -p artifactory restart"
  when:
    - ps_compose.stdout != ""
    - template_compose.changed

- name: "Wait for Artifactory to start"
  uri:
    method: "GET"
    url: "http://localhost/artifactory/api/system/ping"
    validate_certs: False
  register: artifactory_ping
  retries: 6
  delay: 10
  until: "'OK' in artifactory_ping.msg"

- name: "Check Admin Default Password"
  uri:
    method: "GET"
    url: "http://localhost/artifactory/api/system/ping"
    url_username: "{{ artifactory_username }}"
    url_password: "{{ artifactory_default_password }}"
    validate_certs: False
    force_basic_auth: True
  register: artifactory_admin_password
  failed_when: false

- name: "Set Admin Password"
  uri:
    method: "POST"
    url: "http://localhost/artifactory/api/security/users/authorization/changePassword"
    url_username: "{{ artifactory_username }}"
    url_password: "{{ artifactory_default_password }}"
    body_format: "json"
    body: '{"userName":"{{ artifactory_username }}","oldPassword":"{{ artifactory_default_password }}","newPassword1":"{{ artifactory_password }}","newPassword2":"{{ artifactory_password }}"}'
    validate_certs: False
  when: artifactory_admin_password.status == 200

- name: "Get Certs"
  uri:
    method: "GET"
    url: "http://localhost/artifactory/api/system/security/certificates"
    return_content: True
    url_username: "{{ artifactory_username }}"
    url_password: "{{ artifactory_password }}"
    validate_certs: False
  register: artifactory_certs

# TODO: The when conditional isn't very robust. It only checks if certs exist
#       and not if the actual cert we're trying to install exists
# TODO: I had trouble getting the uri module to correctly post the cert pem file
- name: "Add RHEL Cert"
  command: "curl -ku {{ artifactory_username }}:{{ artifactory_password }} -X POST http://localhost/artifactory/api/system/security/certificates/{{ item.name }} -T {{ item.file }}"
  with_items: "{{ artifactory_client_certs }}"
  when:
    - ansible_os_family == 'RedHat'
    - artifactory_certs.json.0.certificateAlias is not defined
    - rh_byol|bool


- name: "Create any repos"
  include_tasks: repos.yml
  when: artifactory_clean|bool
