Artifactory Reverse Proxy
=========

Ansible role to configure the artifactory-pro-nginx docker container.

Requirements
------------

Must be run against a RHEL 7.x host with docker installed. 
Assumes another container named 'artifactory' running JFrog Artifactory on default ports. Probably built with the role 'artifactory'. 


Role Variables
--------------

|  variable            | type   | default                       | description                                                  |
|----------------------|--------|-------------------------------|--------------------------------------------------------------|
|artifactory_rp_data   | string | /opt/data/artifactory_rp_data | Directory where the nginx configuration is placed            |
|artifactory_rp_target | string | artifactory                   | Name of the container hosting the jfrog artifactory instance |
|artifactory_rp_name:  | string | artifactory_revproxy          | Name of the container this builds                            |
|artifactory_rp_image: | string | docker.bintray.io/jfrog/nginx-artifactory-pro:latest | name of the source image to pull      |
|artifactory_rp_user:  | number | 104                           | id of user that the container runs as for permission settings|
|artifactory_rp_group: | number | 107                           | id of group that the container runs as                       | 

Dependencies
------------

N/A

Example Playbook
----------------

    - hosts: artifactory-host
      gather_facts: True
      roles:
         - { role: artifactory }
         - { role: artifactory-reverse-proxy }

License
-------

Apache 2.0

Author Information
------------------

Josiah Ritchie <ritchie.josiah@solute.us>
