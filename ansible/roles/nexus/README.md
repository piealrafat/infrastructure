# Ansible Role: nexus

Ansible role to configure the nexus docker container.

## Requirements

Docker 1.13 or later

## Role Variables
|variable|type|default|description|
|--------|----|-------|-----------|
|docker_compose_binary|string|/usr/local/bin/docker-compose|Directory location of docker compose binary|
|nexus_base_dir|string|/data|Location for the nexus base directory|
|nexus_version|string|6.11.1|Version of nexus to be deployed|
|nexus_container_base|string|docker.bintray.io/jfrog|Name of the nexus container base|
|nexus_container|string|nexus-pro|Name of the nexus container|
|nexus_owner|integer|1030|ID of the owner assigned to nexus|
|nexus_group|integer|1030|ID of the group assigned to nexus|

## Dependencies

None.

## Example Playbook

```yaml
- hosts: artifacts_servers
  roles:
    - docker
    - nexus
```

## License

Apache 2.0

## Author Information

Solute
