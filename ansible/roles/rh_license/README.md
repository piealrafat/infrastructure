Ansible ROle - rh_license
=========

A role to manage Red Hat license entitlements

Requirements
------------

* Server must be RHEL 7.x
* Server must have the subscription-manager binary installed
* Valid RHEL Licenses

Role Variables
--------------

|variable|type|default|description|
|--------|----|-------|-----------|
|rh_byol|boolean|false|when set to true the role will configure the given rhel entitlements|
|rh_activation_key|string|n/a|Activation Key used to subscribe the server|
|rh_pool_ids|string|n/a|hash of pool ids for subscribing to repos|
|rh_org_id|string|n/a|Red Hat Organization ID used to subscribe the server|

Dependencies
------------

N/A

Example Playbook
----------------

```yaml
    - hosts: redhat-servers
      vars:
        rh_activation_key: "{{ vault_ak }}"
        rh_org_id: "{{ vault_org_id }}"
        rh_pool_ids: "{{ vault_pool_ids }}"
        rh_byol: True
      roles:
         - { role: rh_license }
```

License
-------

Apache 2.0

Author Information
------------------

Josh Metheney <metheney.josh@solute.us>
