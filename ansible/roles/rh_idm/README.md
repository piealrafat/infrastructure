Ansible Role - rh_idm
=========

Installs and configures Red Hat Identity Management Service (FreeIPA)

Requirements
------------

* Server must be RHEL 7.x
* Minimum 1 CPU and 1 Gig Ram
* Must have yum repo access to freeipa packages
* Playbook must run gather_facts
* Does not configure HA or clustered servers

Role Variables
--------------

|variable|type|default|description|
|--------|----|-------|-----------|
|rh_idm_packages|list|see defaults|List of RPMs to install for IDM server|
|rh_idm_ds_pass|string|changeme|Directory Server Admin Password|
|rh_idm_admin_pass|string|changeme|Admin user password|
|rh_idm_dns_forwarder|string|8.8.8.8|IP address of an upstream DNS forwarder|
|rh_idm_fqdn|string|n/a|fqdn of the ldap server|
|rh_idm_userou|string|n/a|ldap ou for user objects|
|rh_idm_basedn|string|n/a| base distinguished name to start a search for user|
|rh_idm_groupou|string|n/a| ldap ou for base group for search|

Dependencies
------------

N/A

Example Playbook
----------------

```yaml
    - hosts: idm-servers
      vars:
        rh_idm_ds_pass: "{{ vault_ds_pass }}"
        rh_idm_admin_pass: "{{ vault_admin_pass }}"
        rh_idm_fqdn: "ipa.{{ fqdn }}"
        rh_idm_basedn: "DC={{ fqdn | replace('.',',DC=') }}"
        rh_idm_userou: "CN=users,CN=accounts,{{ rh_idm_basedn }}"
        rh_idm_groupou: "CN=groups,CN=accounts,{{ rh_idm_basedn }}"
        
      roles:
         - { role: rh_idm }
```

License
-------

Apache 2.0

Author Information
------------------

Josh Metheney <metheney.josh@solute.us>
