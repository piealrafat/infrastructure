Simple Repo
===========

This Role handles the creation of a docker-compose-driven artifact solution for installations of the SDK.  It will create a docker-compose file that includes mounts from the image and RPM resources contained in the files directory to the registry:2 and nginx containers that will host them respectively.

Requirements
------------

There are several files required for this role to be run, all of which go into the files/ directory and must, for now, be named as stated below:

- nginx.tar -- Contains a tarred nginx image to be used as the installation yum repo
- registry.tar -- Contains a tarred registry images to be used as the installation registry
- openshift-ose311-tpm-repos-latest.tar.gz -- Contains all the RPMs used during the openshift 3.11 installation.  Created using repsync of the RedHat ose 3.11 yum repo.
- openshift3-registry-data.tar.gz -- Contains all of the images required to complete an openshift 3.11 installation.  Created from list of images described in the openshift3/pipeline.yaml of the sdk/infrastructure/infrastructure gitlab repo.

These files provide all the resources usually retrieved from the internet during an install.

Role Variables
--------------

`docker-compose_binary` - determines path to docker compose binary (Should make this dynamic)

Dependencies
------------

The docker role must be run before this role to ensure docker and docker-compose are present.

Author Information
------------------

Tim Seagren, DevOps Engineer for Solute

email: seagren.tim@solute.us
