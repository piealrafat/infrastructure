Ansible Role - repo
=========

A role to manage repositories on linux system.

Requirements
------------

Currently only runs on Red Hat managed repos through subscription-manager

Role Variables
--------------

|variable|type|default|description|
|--------|----|-------|-----------|
|rh_repos|list|n/a|If set the role will loop over each item and enable the repo using subscription-manager|
|repos_base|list|n/a|If base repos are specified this role will clean any unnecessary repos not included in this list|
|repos_clean|boolean|false|If set to true this role will clean all undefined repos|
|repos_gpg_base_dir|string|/etc/pki/rpm-gpg|Base directory for storing external yum repo gpg keys|


Dependencies
------------

N/A

Example Playbook
----------------

```yaml
    - hosts: redhat-servers
      vars:
        rh_repos:
        - rhel-7-server-rpms
      roles:
         - { role: repo }
```

License
-------

Apache 2.0

Author Information
------------------

Josh Metheney <metheney.josh@solute.us>
