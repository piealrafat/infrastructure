######################################################################
# Clone VM from template for Artifactory server (no additional disks)
######################################################################
resource "vsphere_virtual_machine" "artifactory" {
  name               = "${var.cms_artifactory.hostname}"
  resource_pool_id   = data.vsphere_resource_pool.pool.id
  datastore_id       = data.vsphere_datastore.datastore.id
  num_cpus           = var.cms_artifactory.cpu
  memory             = var.cms_artifactory.memory
  guest_id           = data.vsphere_virtual_machine.template.guest_id
  folder             = var.vmware_folder_name
  wait_for_guest_ip_timeout = "-1"
  network_interface {
    network_id       = data.vsphere_network.network.id
    adapter_type     = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label            = "root"
    size             = var.cms_artifactory.root_vol_size
    unit_number      = "0"
    thin_provisioned = true
  }
  
  disk {
    label            = "raw"
    size             = var.cms_artifactory.additional_disk_size
    unit_number      = "1"
    thin_provisioned = true
  }

  clone {
    template_uuid    = data.vsphere_virtual_machine.template.id
    customize {
      linux_options {
        host_name    = "${var.cms_artifactory.hostname}"
        domain       = var.vmware_vm_domain
      }
      network_interface {
        ipv4_address = var.cms_artifactory_ip_address
        ipv4_netmask = var.vmware_vm_network_ipv4_mask
      }
      ipv4_gateway    = var.vmware_vm_ipv4_gateway
      dns_server_list = [var.vmware_vm_dns_servers]
      dns_suffix_list = [var.vmware_vm_domain]
    }
  }
}

resource "ansible_host" "artifactory" {
  inventory_hostname = "${var.cms_artifactory.hostname}"
  vars = {
    ansible_ssh_host = "${vsphere_virtual_machine.artifactory.default_ip_address}"
  }
  groups = ["artifactory_servers"]
}

output "artifactory_ip_addr" {
  value = "${vsphere_virtual_machine.artifactory.default_ip_address}"
  description = "IP Address of Artifactory server"
}

