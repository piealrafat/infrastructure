######################################################################
## This file is where all Environment Specifc variables are defined ##
## The top section is for VCSA vars and the second part is for VMs ###
######################################################################

######################################################################
###vCenter Server Appliance (VCSA) Environment Specific Information###
######################################################################
#The VCSA User is the user (with domain) to login to VCSA
#Should be defined in a file named creds.tf
#variable "vmware_vcsa_user" {
#  default = ""
#}
#
#The VCSA User password
#Should be defined in a file named creds.tf
#variable "vmware_vcsa_password" {
#  default = ""
#}
#The VCSA FQDN or IP address
variable "vmware_vcsa_server_address" {
  default = "vcenter.contractor.idtus.com"
}
#If VCSA uses self signed certs, set to true. If using offical DoD certs, set to false
variable "vmware_vcsa_self_signed_cert" {
  default = true
}
#The name of the Datacenter in VCSA where the VM should deploy
variable "vmware_datacenter_name" {
  default = "ContractorDC"
}
#The name of the Datastore in VCSA where the VM should reside
variable "vmware_datastore_name" {
  default = "SoluteVMData"
}


#The Resource Pool Name is optional.
#If using Resource Pools, go to provider-vsphere.tf and comment it back in
variable "vmware_resource_pool_name" {
  default = "10.22.1.13/Resources"
}


#The name of the Folder in VCSA where the VM should deploy
variable "vmware_folder_name" {
  default = "Solute"
}
#The Network Adapter matches the drop down in VCSA the name of the Virtual Switch the VM will use
variable "vmware_network_adapter_name" {
  default = "SoluteNetwork"
}
#The name of the base template in VCSA which all VMs are cloned from
variable "vmware_base_template_name" {
  default = "rhel7"

}
######################################################################
###########Virtual Machine Environment Specific Information###########
######################################################################
#The Domain name of the VMs. This will also be the DNS suffix for the VMs
variable "vmware_vm_domain" {
  default = "cms.local"
}
#The IPv4 address of the DNS server utilized by the VMs
variable "vmware_vm_dns_servers" {
  default = "192.168.55.50"
}
#The default IPv4 gateway for the VM
variable "vmware_vm_ipv4_gateway" {
  default = "192.168.55.1"
}
#The IPv4 Address Network Mask in CIDR notation of the VMs. Example: 255.255.255.0 = 24
variable "vmware_vm_network_ipv4_mask" {
  default = "24"
}

# This section defines the FreeIPA server specs, such as hostname, cpu, memory and the root volume size.
variable "cms_freeipa" {
  type = "map"
  default = {
      "hostname" = "cms-freeipa"
      "cpu" = "2"
      "memory" = "4096"
      "root_vol_size" = "200"
  } 
}

# This variable defines the ip addresses to be assigned to the FreeIPA server
variable "cms_freeipa_ip_addresses" {
   type = list(string) 
   default = ["192.168.55.51"]
}

variable "cms_freeipa_count" {
  description = "Number of FreeIPA servers to stand up"
  default = 1
}

# This section defines the OpenShift Container Platform application node server specs, such as hostname, cpu, memory and the root volume size.
variable "cms_ocpapp" {
  type = map
  default = {
      "hostname" = "cms-ocpapp"
      "cpu" = "4"
      "memory" = "16384"
      "root_vol_size" = "200"
    }
}

# This variable defines the ip addresses to be assigned to each of the application nodes
variable "cms_ocpapp_ip_addresses" {
   type = list(string) 
   default = ["192.168.55.52","192.168.55.53","192.168.55.54"]
}

variable "cms_ocpapp_count" {
  description = "Number of OpenShift compute servers to stand up"
  default = 3
}

# This variable defines the artifactory server specs, such as hostname, cpu, memory and the root volume size.
variable "cms_artifactory" {
  type = map
  default = {
      "hostname" = "cms-artifactory"
      "cpu" = "2"
      "memory" = "4096"
      "root_vol_size" = "200"
      "additional_disk_size" = "250"
    }
}

# This variable defines the ip addresses to be assigned to the artifactory server
variable "cms_artifactory_ip_address" {
   type = string 
   default = "192.168.55.55"
}

# This variable defines the OpenShift Container Platform master node server specs, such as hostname, cpu, memory and the root volume size.
variable "cms_ocpmaster" {
  type = map
  default =  {
      "hostname" = "cms-ocpmaster"
      "cpu" = "4"
      "memory" = "16384"
      "root_vol_size" = "200"
      "additional_disk_size" = "250"
    }
}

# This variable defines the ip addresses to be assigned to each of the master nodes
variable "cms_ocpmaster_ip_addresses" {
   type = list(string) 
   default = ["192.168.55.5"]
}

variable "cms_ocpmaster_count" {
  description = "Number of OpenShift master servers to stand up"
  default = 1
}
