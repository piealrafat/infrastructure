######################################################################
#### This file tells Terraform the minimum version of TF required ####
######################################################################
terraform {
  required_version = ">= 0.12"
}
