######################################################################
# Clone VM from template for OCP Master server (no additional disks)
######################################################################
resource "vsphere_virtual_machine" "ocpmaster" {
  name               = "${var.cms_ocpmaster.hostname}-${count.index}"
  count              = var.cms_ocpmaster_count
  resource_pool_id   = data.vsphere_resource_pool.pool.id
  datastore_id       = data.vsphere_datastore.datastore.id
  num_cpus           = var.cms_ocpmaster.cpu
  memory             = var.cms_ocpmaster.memory
  guest_id           = data.vsphere_virtual_machine.template.guest_id
  folder             = var.vmware_folder_name
  network_interface {
    network_id       = data.vsphere_network.network.id
    adapter_type     = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label            = "root"
    size             = var.cms_ocpmaster.root_vol_size
    unit_number      = "0"
    thin_provisioned = true
  }

  disk {
    label            = "raw"
    size             = var.cms_ocpmaster.additional_disk_size
    unit_number      = "1"
    thin_provisioned = true
  }

  clone {
    template_uuid    = data.vsphere_virtual_machine.template.id
    customize {
      linux_options {
        host_name    = "${var.cms_ocpmaster.hostname}-${count.index}"
        domain       = var.vmware_vm_domain
      }
      network_interface {
        ipv4_address = var.cms_ocpmaster_ip_addresses[count.index]
        ipv4_netmask = var.vmware_vm_network_ipv4_mask
      }
      ipv4_gateway    = var.vmware_vm_ipv4_gateway
      dns_server_list = [var.vmware_vm_dns_servers]
      dns_suffix_list = [var.vmware_vm_domain]
    }
  }
}

######################################################################
# Clone VM from template for OCP App server (no additional disks)
######################################################################
resource "vsphere_virtual_machine" "ocpapp" {
  name               = "${var.cms_ocpapp.hostname}-${count.index}"
  count              = var.cms_ocpapp_count
  resource_pool_id   = data.vsphere_resource_pool.pool.id
  datastore_id       = data.vsphere_datastore.datastore.id
  num_cpus           = var.cms_ocpapp.cpu
  memory             = var.cms_ocpapp.memory
  guest_id           = data.vsphere_virtual_machine.template.guest_id
  folder             = var.vmware_folder_name
  network_interface {
    network_id       = data.vsphere_network.network.id
    adapter_type     = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label            = "root"
    size             = var.cms_ocpapp.root_vol_size
    unit_number      = "0"
    thin_provisioned = true
  }
  
  clone {
    template_uuid    = data.vsphere_virtual_machine.template.id
    customize {
      linux_options {
        host_name    = "${var.cms_ocpapp.hostname}-${count.index}"
        domain       = var.vmware_vm_domain
      }
      network_interface {
        ipv4_address = var.cms_ocpapp_ip_addresses[count.index]
        ipv4_netmask = var.vmware_vm_network_ipv4_mask
      }
      ipv4_gateway    = var.vmware_vm_ipv4_gateway
      dns_server_list = [var.vmware_vm_dns_servers]
      dns_suffix_list = [var.vmware_vm_domain]
    }
  }
}

resource "ansible_host" "ose-master" {
  count = "${var.cms_ocpmaster_count}"
  inventory_hostname = "${var.cms_ocpmaster.hostname}-${count.index}" 
  vars = {
    ansible_ssh_host = "${element(vsphere_virtual_machine.ocpmaster.*.default_ip_address, count.index)}"
    openshift_node_group_name = "node-config-master-infra"
  }
  groups = ["masters","etcd","nodes","glusterfs"]
}

resource "ansible_host" "ose-node" {
  count = "${var.cms_ocpapp_count}"
  inventory_hostname = "${var.cms_ocpapp.hostname}-${count.index}"
  vars = {
    ansible_ssh_host = "${element(vsphere_virtual_machine.ocpapp.*.default_ip_address, count.index)}"
    openshift_node_group_name = "node-config-compute"
  }
  groups = ["nodes"]
}

resource "ansible_group" "ose" {
  inventory_group_name = "OSEv3"
  children = ["masters", "nodes", "etcd", "glusterfs"]
}

output "osemaster_ip_addr" {
value = ["${vsphere_virtual_machine.ocpmaster.*.default_ip_address}"]
  description = "IP Address of OSE Master servers"
}

output "oseapp_ip_addr" {
value = ["${vsphere_virtual_machine.ocpapp.*.default_ip_address}"]
  description = "IP Address of OSE Compute servers"
}
