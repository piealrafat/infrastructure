######################################################################
### This file defines all vSphere provider components needed by TF ###
######################################################################
provider "vsphere" {
  user           = "rrahman@idtstacs.local"
  password       = "OCzdjUk0@9*5"
  vsphere_server = "vcenter.idtstacs.com"
  allow_unverified_ssl = var.vmware_vcsa_self_signed_cert
}

#Data Sources
data "vsphere_datacenter" "dc" {
  name = var.vmware_datacenter_name
}

data "vsphere_datastore" "datastore" {
  name          = var.vmware_datastore_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_compute_cluster" "cluster" {
  name          = var.vmware_cluster_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name		= "${var.vmware_resource_pool_name}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name          = var.vmware_network_adapter_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "template" {
  name          = var.vmware_base_template_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

