######################################################################
# Clone VM from template for FreeIPA server (no additional disks)
######################################################################
resource "vsphere_virtual_machine" "freeipa" {
  name               = "${var.cms_freeipa.hostname}-${count.index}"
  count              = var.cms_freeipa_count
  resource_pool_id   = data.vsphere_resource_pool.pool.id
  datastore_id       = data.vsphere_datastore.datastore.id
  num_cpus           = var.cms_freeipa.cpu
  memory             = var.cms_freeipa.memory
  guest_id           = data.vsphere_virtual_machine.template.guest_id
  folder             = var.vmware_folder_name
  network_interface {
    network_id       = data.vsphere_network.network.id
    adapter_type     = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label            = "root"
    size             = var.cms_freeipa.root_vol_size
#   unit_number      = "0"
    thin_provisioned = true
  }

  clone {
    template_uuid    = data.vsphere_virtual_machine.template.id
    customize {
      linux_options {
        host_name    = "${var.cms_freeipa.hostname}-${count.index}"
        domain       = var.vmware_vm_domain
      }
      network_interface {
        ipv4_address = var.cms_freeipa_ip_addresses[count.index]
        ipv4_netmask = var.vmware_vm_network_ipv4_mask
      }
      ipv4_gateway    = var.vmware_vm_ipv4_gateway
      dns_server_list = [var.vmware_vm_dns_servers]
      dns_suffix_list = [var.vmware_vm_domain]
    }
  }


# TODO: Migrating this task to cloudinit
#  provisioner "file" {
#    source      = "../../keys/id_rsa.pub"
#    destination = "/root/.ssh/"
#    
#    connection {
#      type     = "ssh"
#      user     = "${var.vmware_vcsa_user}"
#      password = "${var.vmware_vcsa_password}"
#      host     = "${var.cms_freeipa_ip_addresses[count.index]}"
#    }
#  }
#  
#  provisioner "remote-exec" {
#    inline = [
#      "touch /root/.ssh/authorized_keys",
#      "cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys"
#    ]
#    
#    connection {
#      type     = "ssh"
#      user     = "${var.vmware_vcsa_user}"
#      password = "${var.vmware_vcsa_password}"
#      host     = "${var.cms_freeipa_ip_addresses[count.index]}"
#    }
#  }
}

resource "ansible_host" "ipa" {
  count = "${var.cms_freeipa_count}"
  inventory_hostname = "${var.cms_freeipa.hostname}-${count.index}"
  vars = {
    ansible_ssh_host = "${element(vsphere_virtual_machine.freeipa.*.default_ip_address, count.index)}"
  }
  groups = ["ipa_servers"]
}

output "ipa_ip_addr" {
  value = ["${vsphere_virtual_machine.freeipa.*.default_ip_address}"]
  description = "IP Address of FreeIPA server"
}
