data "aws_availability_zones" "available" {}

variable "aws_region" {
  default = "us-gov-east-1"
}

variable "aws_endpoint_ec2" {
  default = "ec2.us-gov-east-1.amazonaws.com"
}

variable "aws_endpoint_iam" {
  default = "iam.us-gov.amazonaws.com"
}

variable "aws_endpoint_elb" {
  default = "elasticloadbalancing.us-gov-east-1.amazonaws.com"
}

variable "aws_endpoint_s3" {
  default = "s3-fips.us-gov-east-1.amazonaws.com"
}

variable "vpc_cidr_block" {
  default = "172.16.0.0/16"
}

variable "project_name" {
  default = "main"
}

variable "project_fqdn" {
  default = "cms.solute.us"
}

variable "ipa_ami" {
  default = "ami-ccc120bd"
}

variable "vpn_ami" {
  default = "ami-ccc120bd"
}

variable "ipa_type" {
  default = "t3.medium"
}

variable "vpn_type" {
  default = "t3.small"
}

variable "ose_ami" {
  default = "ami-ccc120bd"
}

variable "ose_master_type" {
  default = "m5.xlarge"
}

variable "ose_master_count" {
  default = 3
}

variable "ose_master_root_volume_size" {
  default = 100
}

variable "ose_master_root_volume_type" {
  default = "gp2"
}

variable "ose_node_root_volume_size" {
  default = 100
}

variable "ose_node_root_volume_type" {
  default = "gp2"
}

variable "ose_gluster_data_volume_size" {
  default = 500
}

variable "ose_gluster_data_volume_type" {
  default = "gp2"
}

variable "ose_gluster_data_volume_path" {
  default = "/dev/xvdf"
}

variable "ose_node_type" {
  default = "m5.xlarge"
}

variable "ose_node_count" {
  default = 8
}

variable "artifactory_integration_count" {
  default = 1
}

variable "artifactory_ami" {
  default = "ami-ccc120bd"
}

variable "artifactory_type" {
  default = "t3.medium"
}

variable "artifactory_host_root_volume_size" {
  default = 100
}

variable "artifactory_host_root_volume_type" {
  default = "gp2"
}

variable "artifactory_host_data_volume_size" {
  default = 500
}

variable "artifactory_host_data_volume_type" {
  default = "gp2"
}

variable "artifactory_host_data_volume_path" {
  default = "/dev/xvdf"
}

variable "cjmtk_ami" {
  default = "ami-ccc120bd"
}

variable "cjmtk_geo_node_count" {
  default = 1
}

variable "cjmtk_geo_type" {
  default = "t3.xlarge"
}

variable "cjmtk_geo_host_root_volume_size" {
  default = 250
}

variable "cjmtk_geo_host_root_volume_type" {
  default = "gp2"
}

variable "cjmtk_gis_node_count" {
  default = 1
}

variable "cjmtk_gis_type" {
  default = "t3.2xlarge"
}

variable "cjmtk_gis_host_root_volume_size" {
  default = 500
}

variable "cjmtk_gis_host_root_volume_type" {
  default = "gp2"
}

variable "cjmtk_ds_node_count" {
  default = 1
}

variable "cjmtk_ds_type" {
  default = "t3.xlarge"
}

variable "cjmtk_ds_host_root_volume_size" {
  default = 500
}

variable "cjmtk_ds_host_root_volume_type" {
  default = "gp2"
}

variable "cjmtk_proxy_node_count" {
  default = 1
}

variable "cjmtk_proxy_type" {
  default = "t3.xlarge"
}

variable "cjmtk_proxy_host_root_volume_size" {
  default = 100
}

variable "cjmtk_proxy_host_root_volume_type" {
  default = "gp2"
}

variable "existing_vpc_id" {
  default = null
}

variable "use_peering" {
  default = false
}
