terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "cmsp-backup"
    key = "dev/terraform/terraform.tfstate"
    region = "us-gov-east-1"
  }
}