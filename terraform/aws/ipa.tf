#ipa
resource "aws_security_group" "ipa-sg" {
  name = "ipa-sg"
  description = "Security Group for ID Manager Hosts"
 vpc_id = "${aws_vpc.main.id}"
}

resource "aws_security_group_rule" "ipa-22-ingress" {
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-80-ingress" {
  type = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-443-ingress" {
  type = "ingress"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-389-ingress" {
  type = "ingress"
  from_port = 389
  to_port = 389
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-636-ingress" {
  type = "ingress"
  from_port = 636
  to_port = 636
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-88t-ingress" {
  type = "ingress"
  from_port = 88
  to_port = 88
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-464t-ingress" {
  type = "ingress"
  from_port = 464
  to_port = 464
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-53t-ingress" {
  type = "ingress"
  from_port = 53
  to_port = 53
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-88-ingress" {
  type = "ingress"
  from_port = 88
  to_port = 88
  protocol = "udp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-464-ingress" {
  type = "ingress"
  from_port = 464
  to_port = 464
  protocol = "udp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-53-ingress" {
  type = "ingress"
  from_port = 53
  to_port = 53
  protocol = "udp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-123-ingress" {
  type = "ingress"
  from_port = 123
  to_port = 123
  protocol = "udp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "ipa-egress" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_instance" "ipa-host" {
  ami = "${var.ipa_ami}"
  instance_type = "${var.ipa_type}"
  subnet_id = "${aws_subnet.mgmt-1.id}"
  vpc_security_group_ids = ["${aws_security_group.ipa-sg.id}"]
  key_name = "default"
  tags = {
    Name ="ipa",
    Project = "${var.project_name}"
    Role = "ipa_servers"
  }
}

resource "aws_vpc_dhcp_options" "ipa-dhcp" {
  domain_name_servers = ["${aws_instance.ipa-host.private_ip}"]
  domain_name = "${var.project_fqdn}"
  ntp_servers = ["${aws_instance.ipa-host.private_ip}"]
  tags = {
    Name = "ipa-dhcp-opts"
    Project = "${var.project_name}"
  }
}

resource "aws_vpc_dhcp_options_association" "ipa-dhcp-assoc" {
  vpc_id = "${aws_vpc.main.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.ipa-dhcp.id}"
}

output "ipa_priv_ip_addr" {
  value = "${aws_instance.ipa-host.private_ip}"
  description = "The Private IP address of the IPA host"
}

resource "ansible_host" "ipa" {
  inventory_hostname = "ipa"
  vars = {
    ansible_ssh_host = "${aws_instance.ipa-host.private_ip}"
  }
  groups = ["ipa_servers"]
}
