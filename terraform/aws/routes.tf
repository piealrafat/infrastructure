#default route
resource "aws_route" "default" {
  route_table_id = "${aws_vpc.main.main_route_table_id}"
  gateway_id = "${aws_internet_gateway.main.id}"
  destination_cidr_block = "0.0.0.0/0"
}

#private route table
resource "aws_route_table" "private-rt" {
  vpc_id = "${aws_vpc.main.id}"
  tags = {
    Name ="private-rt-${var.project_name}",
    Project = "${var.project_name}"
  }
}

resource  "aws_route" "private-rt-nat" {
    route_table_id = "${aws_route_table.private-rt.id}"
    destination_cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.natgw.id}"
}

# TODO: Simplify this to use the count var and loop over the subnet list
resource "aws_route_table_association" "mgmt1-rt" {
  subnet_id = "${aws_subnet.mgmt-1.id}"
  route_table_id = "${aws_route_table.private-rt.id}"
}

resource "aws_route_table_association" "mgmt2-rt" {
  subnet_id = "${aws_subnet.mgmt-2.id}"
  route_table_id = "${aws_route_table.private-rt.id}"
}

resource "aws_route_table_association" "mgmt3-rt" {
  subnet_id = "${aws_subnet.mgmt-3.id}"
  route_table_id = "${aws_route_table.private-rt.id}"
}

resource "aws_route_table_association" "services1-rt" {
  subnet_id = "${aws_subnet.services-1.id}"
  route_table_id = "${aws_route_table.private-rt.id}"
}

resource "aws_route_table_association" "services2-rt" {
  subnet_id = "${aws_subnet.services-2.id}"
  route_table_id = "${aws_route_table.private-rt.id}"
}

resource "aws_route_table_association" "services3-rt" {
  subnet_id = "${aws_subnet.services-3.id}"
  route_table_id = "${aws_route_table.private-rt.id}"
}
