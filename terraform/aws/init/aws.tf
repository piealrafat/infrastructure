terraform {
  required_version = ">= 0.12"
}

provider "aws" {
    region = "us-gov-east-1"
}

resource "aws_s3_bucket" "remote-state-s3" {
  bucket = "cmsp-backup"
  acl = "private"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
