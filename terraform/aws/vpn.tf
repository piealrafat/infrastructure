#vpn
resource "aws_security_group" "vpn-sg" {
  name = "vpn-sg"
  description = "Security Group for VPN Hosts"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 943
    to_port = 943
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 1194
    to_port = 1194
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "vpn-eip" {
  instance = "${aws_instance.vpn-host.id}"
}

resource "aws_instance" "vpn-host" {
  ami = "${var.vpn_ami}"
  instance_type = "${var.vpn_type}"
  subnet_id = "${aws_subnet.public-1.id}"
  vpc_security_group_ids = ["${aws_security_group.vpn-sg.id}"]
  key_name = "default"
  tags = {
    Name ="vpn",
    Project = "${var.project_name}"
    Role = "vpn-servers"
  }
}

resource "ansible_host" "vpn" {
  inventory_hostname = "vpn"
  vars = {
    ansible_ssh_host = "${aws_instance.vpn-host.private_ip}"
    openvpnas_private_net = "${var.vpc_cidr_block}"
    openvpnas_public_ip = "${aws_eip.vpn-eip.public_ip}"
  }
  groups = ["vpn-servers"]
}
