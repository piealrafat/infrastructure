provider "aws" {
    region = "${var.aws_region}"
    endpoints {
      ec2 = "${var.aws_endpoint_ec2}"
      iam = "${var.aws_endpoint_iam}"
      elb = "${var.aws_endpoint_elb}"
      s3 = "${var.aws_endpoint_s3}"
    }
}