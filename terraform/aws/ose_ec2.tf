resource "aws_instance" "ose-master-host" {
  ami = "${var.ose_ami}"
  instance_type = "${var.ose_master_type}"
  subnet_id = "${aws_subnet.services-1.id}"
  count = "${var.ose_master_count}"
  iam_instance_profile = "${aws_iam_instance_profile.ose_master_profile.name}"
  vpc_security_group_ids = [
    "${aws_security_group.ose-sg.id}",
    "${aws_security_group.ose-etcd-sg.id}",
    "${aws_security_group.ose-infra-sg.id}",
    "${aws_security_group.ose-gluster-sg.id}",
    "${aws_security_group.ose-master-sg.id}",
    "${aws_security_group.ose-node-sg.id}"
  ]
  key_name = "${aws_key_pair.default.key_name}"
  root_block_device {
    volume_size = "${var.ose_master_root_volume_size}"
    volume_type = "${var.ose_master_root_volume_type}"
  }
  ebs_block_device {
    volume_size = "${var.ose_gluster_data_volume_size}"
    volume_type = "${var.ose_gluster_data_volume_type}"
    device_name = "${var.ose_gluster_data_volume_path}"
  }
  tags = {
    Name ="OSE Master ${count.index}"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
  depends_on = ["aws_vpc_dhcp_options.ipa-dhcp"]
}

output "ose_master_priv_ip_addr" {
  value = "${aws_instance.ose-master-host.*.private_ip}"
  description = "The Private IP address of the VPN host"
}


resource "aws_instance" "ose-node-host" {
  ami = "${var.ose_ami}"
  instance_type = "${var.ose_node_type}"
  subnet_id = "${aws_subnet.services-1.id}"
  count = "${var.ose_node_count}"
  iam_instance_profile = "${aws_iam_instance_profile.ose_node_profile.name}"
  vpc_security_group_ids = [
    "${aws_security_group.ose-sg.id}",
    "${aws_security_group.ose-node-sg.id}"
  ]
  key_name = "${aws_key_pair.default.key_name}"
  root_block_device {
    volume_size = "${var.ose_node_root_volume_size}"
    volume_type = "${var.ose_node_root_volume_type}"
  }
  tags = {
    Name ="OSE Node ${count.index}"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
  depends_on = ["aws_vpc_dhcp_options.ipa-dhcp"]
}
