#bastion host
resource "aws_security_group" "bastion-sg" {
  name = "bastion-sg"
  description = "Security Group for Bastion Hosts"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "bastion-eip" {
  instance = "${aws_instance.bastion-host.id}"
}

resource "aws_instance" "bastion-host" {
  ami = "ami-fdaf4e8c"
  instance_type = "t3.micro"
  subnet_id = "${aws_subnet.public-1.id}"
  vpc_security_group_ids = ["${aws_security_group.bastion-sg.id}"]
  key_name = "${aws_key_pair.default.key_name}"
  tags = {
    Name ="bastion",
    Project = "${var.project_name}"
    Role = "bastion_servers"
  }
}

output "bastion_ip_addr" {
  value = "${aws_eip.bastion-eip.public_ip}"
  description = "The Public IP address of the bastion host"
}

resource "ansible_host" "bastion" {
  inventory_hostname = "bastion"
  vars = {
    ansible_ssh_host = "${aws_eip.bastion-eip.public_ip}"
  }
  groups = ["bastion_servers"]
}
