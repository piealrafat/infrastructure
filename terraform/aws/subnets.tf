#public subnets

## TODO: Simplify the subnet definitions by using count = len(${data.aws_availability_zones.available})
### will need to update names on all resources using subnet ids
### will need to update the availability_zone = to use count instead of specific index
resource "aws_subnet" "public-1" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  cidr_block = "${cidrsubnet(var.vpc_cidr_block, 8, 1)}"
  tags = {
    Name ="sub-pub1-${var.project_name}",
    Project = "${var.project_name}"
  }
}

resource "aws_subnet" "public-2" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  cidr_block = "${cidrsubnet(var.vpc_cidr_block, 8, 2)}"
  tags = {
    Name ="sub-pub2-${var.project_name}",
    Project = "${var.project_name}"
  }
}

resource "aws_subnet" "public-3" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[2]}"
  cidr_block = "${cidrsubnet(var.vpc_cidr_block, 8, 3)}"
  tags = {
    Name ="sub-pub3-${var.project_name}",
    Project = "${var.project_name}"
  }
}

#management subnets (private)
resource "aws_subnet" "mgmt-1" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  cidr_block = "${cidrsubnet(var.vpc_cidr_block, 8, 4)}"
  tags = {
    Name ="sub-mgmt1-${var.project_name}",
    Project = "${var.project_name}"
  }
}

resource "aws_subnet" "mgmt-2" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  cidr_block = "${cidrsubnet(var.vpc_cidr_block, 8, 5)}"
  tags = {
    Name ="sub-mgmt2-${var.project_name}",
    Project = "${var.project_name}"
  }
}

resource "aws_subnet" "mgmt-3" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[2]}"
  cidr_block = "${cidrsubnet(var.vpc_cidr_block, 8, 6)}"
  tags = {
    Name ="sub-mgmt3-${var.project_name}",
    Project = "${var.project_name}"
  }
}

#services subnets (private)
resource "aws_subnet" "services-1" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  cidr_block = "${cidrsubnet(var.vpc_cidr_block, 8, 7)}"
  tags = {
    Name ="sub-services1-${var.project_name}",
    Project = "${var.project_name}"
  }
}

resource "aws_subnet" "services-2" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  cidr_block = "${cidrsubnet(var.vpc_cidr_block, 8, 8)}"
  tags = {
    Name ="sub-services2-${var.project_name}",
    Project = "${var.project_name}"
  }
}

resource "aws_subnet" "services-3" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[2]}"
  cidr_block = "${cidrsubnet(var.vpc_cidr_block, 8, 9)}"
  tags = {
    Name ="sub-services3-${var.project_name}",
    Project = "${var.project_name}"
  }
}