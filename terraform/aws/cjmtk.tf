#cjmtk
resource "aws_security_group" "cjmtk-sg" {
  name        = "cjmtk_sg"
  description = "Security Group for cjmtk Hosts"
  vpc_id      = "${aws_vpc.main.id}"

  # TODO: Create more restrictive fw rules
  # Allow all from VPC
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }
  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

## GeoEvent Servers
resource "aws_instance" "cjmtk-geo-host" {
  ami = "${var.cjmtk_ami}"
  instance_type = "${var.cjmtk_geo_type}"
  count = "${var.cjmtk_geo_node_count}"
  subnet_id = "${aws_subnet.services-1.id}"
  vpc_security_group_ids = ["${aws_security_group.cjmtk-sg.id}"]
  key_name = "${aws_key_pair.default.key_name}"
  root_block_device {
    volume_size = "${var.cjmtk_geo_host_root_volume_size}"
    volume_type = "${var.cjmtk_geo_host_root_volume_type}"
  }
  tags = {
    Name ="CJMTK GeoEvent ${count.index}",
    Project = "${var.project_name}"
  }
}

output "cjmtk_geo_priv_ip_addr" {
    value = "${aws_instance.cjmtk-geo-host.*.private_ip}"
    description = "The Private IP address of the cjmtk geo host"
}

resource "ansible_host" "cjmtk-geo" {
  count = "${var.cjmtk_geo_node_count}"
  inventory_hostname = "cjmtk-geo-${count.index}"
  vars = {
    ansible_ssh_host = "${element(aws_instance.cjmtk-geo-host.*.private_ip, count.index)}"
  }
  groups = ["cjmtk_servers","cjmtk_geo_servers"]
}


## GIS Servers
resource "aws_instance" "cjmtk-gis-host" {
  ami = "${var.cjmtk_ami}"
  instance_type = "${var.cjmtk_gis_type}"
  count = "${var.cjmtk_gis_node_count}"
  subnet_id = "${aws_subnet.services-1.id}"
  vpc_security_group_ids = ["${aws_security_group.cjmtk-sg.id}"]
  key_name = "${aws_key_pair.default.key_name}"
  root_block_device {
    volume_size = "${var.cjmtk_gis_host_root_volume_size}"
    volume_type = "${var.cjmtk_gis_host_root_volume_type}"
  }
  tags = {
    Name ="CJMTK GIS ${count.index}",
    Project = "${var.project_name}"
  }
}

output "cjmtk_gis_priv_ip_addr" {
    value = "${aws_instance.cjmtk-gis-host.*.private_ip}"
    description = "The Private IP address of the cjmtk gis host"
}

resource "ansible_host" "cjmtk-gis" {
  count = "${var.cjmtk_gis_node_count}"
  inventory_hostname = "cjmtk-gis-${count.index}"
  vars = {
    ansible_ssh_host = "${element(aws_instance.cjmtk-gis-host.*.private_ip, count.index)}"
  }
  groups = ["cjmtk_servers","cjmtk_gis_servers"]
}


## Spatiotemporal Data Store Servers
resource "aws_instance" "cjmtk-ds-host" {
  ami = "${var.cjmtk_ami}"
  instance_type = "${var.cjmtk_ds_type}"
  count = "${var.cjmtk_ds_node_count}"
  subnet_id = "${aws_subnet.services-1.id}"
  vpc_security_group_ids = ["${aws_security_group.cjmtk-sg.id}"]
  key_name = "${aws_key_pair.default.key_name}"
  root_block_device {
    volume_size = "${var.cjmtk_ds_host_root_volume_size}"
    volume_type = "${var.cjmtk_ds_host_root_volume_type}"
  }
  tags = {
    Name ="CJMTK DataStore ${count.index}",
    Project = "${var.project_name}"
  }
}

output "cjmtk_ds_priv_ip_addr" {
    value = "${aws_instance.cjmtk-ds-host.*.private_ip}"
    description = "The Private IP address of the cjmtk ds host"
}

resource "ansible_host" "cjmtk-ds" {
  count = "${var.cjmtk_ds_node_count}"
  inventory_hostname = "cjmtk-ds-${count.index}"
  vars = {
    ansible_ssh_host = "${element(aws_instance.cjmtk-ds-host.*.private_ip, count.index)}"
  }
  groups = ["cjmtk_servers","cjmtk_ds_servers"]
}


## Proxy Servers
# TODO: Determine if these can be replaced by ELBs
resource "aws_instance" "cjmtk-proxy-host" {
  ami = "${var.cjmtk_ami}"
  instance_type = "${var.cjmtk_proxy_type}"
  count = "${var.cjmtk_proxy_node_count}"
  subnet_id = "${aws_subnet.services-1.id}"
  vpc_security_group_ids = ["${aws_security_group.cjmtk-sg.id}"]
  key_name = "${aws_key_pair.default.key_name}"
  root_block_device {
    volume_size = "${var.cjmtk_proxy_host_root_volume_size}"
    volume_type = "${var.cjmtk_proxy_host_root_volume_type}"
  }
  tags = {
    Name ="CJMTK proxy ${count.index}",
    Project = "${var.project_name}"
  }
}

output "cjmtk_proxy_priv_ip_addr" {
    value = "${aws_instance.cjmtk-proxy-host.*.private_ip}"
    description = "The Private IP address of the cjmtk proxy host"
}

resource "ansible_host" "cjmtk-proxy" {
  count = "${var.cjmtk_proxy_node_count}"
  inventory_hostname = "cjmtk-proxy-${count.index}"
  vars = {
    ansible_ssh_host = "${element(aws_instance.cjmtk-proxy-host.*.private_ip, count.index)}"
  }
  groups = ["cjmtk_servers","cjmtk_proxy_servers"]
}
