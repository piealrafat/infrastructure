#artifactory
resource "aws_security_group" "artifactory-sg" {
  name        = "artifactory_sg"
  description = "Security Group for artifactory Hosts"
  vpc_id      = "${aws_vpc.main.id}"
}

  # SSH access from anywhere
resource "aws_security_group_rule" "artifactory-22-ingress" {
  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.artifactory-sg.id}"
}

  # HTTP access from the VPC
resource "aws_security_group_rule" "artifactory-80-ingress" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.artifactory-sg.id}"
}

  # HTTPS access from the VPC
resource "aws_security_group_rule" "artifactory-443-ingress" {
  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.artifactory-sg.id}"
}
  #TODO determine reasoning for port 10001
  # HTTPS access from the VPC
resource "aws_security_group_rule" "artifactory-10001-ingress" {
  type        = "ingress"
  from_port   = 10001
  to_port     = 10001
  protocol    = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.artifactory-sg.id}"
}
  #TODO determine reasoning for port 8081
  # HTTP access from the VPC
resource "aws_security_group_rule" "artifactory-8081-ingress" {
  type        = "ingress"
  from_port   = 8081
  to_port     = 8081
  protocol    = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.artifactory-sg.id}"
}

  # outbound internet access
resource "aws_security_group_rule" "artifactory-0-egress" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.artifactory-sg.id}"
}

resource "aws_ebs_volume" "artifactory-data" {
  availability_zone = "${aws_instance.artifactory-host.availability_zone}"
  size = "${var.artifactory_host_data_volume_size}"
  type = "${var.artifactory_host_data_volume_type}"
  encrypted = true
  tags = {
    Name = "artifactory-data"
    Project = "${var.project_name}"
  }
}

resource "aws_volume_attachment" "artifactory-data-attachment" {
  device_name = "${var.artifactory_host_data_volume_path}"
  volume_id = "${aws_ebs_volume.artifactory-data.id}"
  instance_id = "${aws_instance.artifactory-host.id}"
}

resource "aws_instance" "artifactory-host" {
  ami = "${var.artifactory_ami}"
  instance_type = "${var.artifactory_type}"
  subnet_id = "${aws_subnet.services-1.id}"
  vpc_security_group_ids = ["${aws_security_group.artifactory-sg.id}"]
  key_name = "default"
  root_block_device {
    volume_size = "${var.artifactory_host_root_volume_size}"
    volume_type = "${var.artifactory_host_root_volume_type}"
  }
  tags = {
    Name ="artifactory",
    Project = "${var.project_name}"
  }
}

resource "aws_ebs_volume" "artifactory-integration-data" {
  availability_zone = "${aws_instance.artifactory-integration-host.0.availability_zone}"
  size = "${var.artifactory_host_data_volume_size}"
  type = "${var.artifactory_host_data_volume_type}"
  encrypted = true
  tags = {
    Name = "artifactory-integration-data"
    Project = "${var.project_name}"
  }
}

resource "aws_volume_attachment" "artifactory-integration-data-attachment" {
  device_name = "${var.artifactory_host_data_volume_path}"
  volume_id = "${aws_ebs_volume.artifactory-integration-data.id}"
  instance_id = "${aws_instance.artifactory-integration-host.0.id}"
}

resource "aws_instance" "artifactory-integration-host" {
  count = "${var.artifactory_integration_count}"
  ami = "${var.artifactory_ami}"
  instance_type = "${var.artifactory_type}"
  subnet_id = "${aws_subnet.services-1.id}"
  vpc_security_group_ids = ["${aws_security_group.artifactory-sg.id}"]
  key_name = "default"
  root_block_device {
    volume_size = "${var.artifactory_host_root_volume_size}"
    volume_type = "${var.artifactory_host_root_volume_type}"
  }
  tags = {
    Name ="artifactory-integration",
    Project = "${var.project_name}"
  }
}

output "artifactory_priv_ip_addr" {
    value = "${aws_instance.artifactory-host.private_ip}"
    description = "The Private IP address of the artifactory host"
}

resource "ansible_host" "artifactory" {
  inventory_hostname = "artifacts"
  vars = {
    ansible_ssh_host = "${aws_instance.artifactory-host.private_ip}"
  }
  groups = ["artifactory_servers"]
}

resource "ansible_host" "artifactory-integration" {
  count = "${var.artifactory_integration_count}"
  inventory_hostname = "artifacts-int"
  vars = {
    ansible_ssh_host = "${aws_instance.artifactory-integration-host.0.private_ip}"
  }
  groups = ["artifactory_servers"]
}
