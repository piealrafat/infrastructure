resource "aws_security_group" "ose-sg" {
  name = "ose-sg"
  description = "Security Group for OSE Hosts"
  vpc_id = "${aws_vpc.main.id}"
  tags = {
    Name ="OSE SG"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
}

resource "aws_security_group_rule" "ingress-22-ose" {
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ose-sg.id}"
}

resource "aws_security_group_rule" "ingess-10010-ose" {
  type = "ingress"
  from_port = 10010
  to_port = 10010
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr_block}"]
  security_group_id = "${aws_security_group.ose-sg.id}"
}

resource "aws_security_group_rule" "egress-ose" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ose-sg.id}"
}

resource "aws_security_group" "ose-master-sg" {
  name = "ose-master-sg"
  description = "Security Group for OSE Master Hosts"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = 8443
    to_port = 8443
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }
  tags = {
    Name ="OSE Master SG"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
}

resource "aws_security_group" "ose-etcd-sg" {
  name = "ose-etcd-sg"
  description = "Security Group for OSE etcd Hosts"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = 2379
    to_port = 2379
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }
  ingress {
    from_port = 2380
    to_port = 2380
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }
  tags = {
    Name ="OSE ETCD SG"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
}

resource "aws_security_group" "ose-node-sg" {
  name = "ose-node-sg"
  description = "Security Group for OSE node Hosts"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = 10250
    to_port = 10250
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }
  ingress {
    from_port = 4789
    to_port = 4789
    protocol = "udp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }
  tags = {
    Name ="OSE Node SG"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
}

resource "aws_security_group" "ose-infra-sg" {
  name = "ose-infra-sg"
  description = "Security Group for OSE infra Hosts"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }
  tags = {
    Name ="OSE Infra SG"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
}

resource "aws_security_group" "ose-gluster-sg" {
  name = "ose-gluster-sg"
  description = "Security Group for OSE gluster Hosts"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["${var.vpc_cidr_block}"]
  }
  tags = {
    Name ="OSE gluster SG"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
}
