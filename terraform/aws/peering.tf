#VPC peering connection from existing VPC that SWFTS Builder AMI is within to soon to be created vpc-main with OSE resources. To enable set 'TF_VAR_use_peering=1' & 'TF_VAR_existing_vpc_id="$(aws ec2 describe-instances --instance-id $(curl http://169.254.169.254/latest/meta-data/instance-id) --query 'Reservations[0].Instances[0].NetworkInterfaces[0].VpcId' | tr -d '"')"' in shell wherever terraform files/binary are located
resource "aws_vpc_peering_connection" "main" {
  count       = "${var.use_peering ? 1 : 0}"
  peer_vpc_id = "${var.existing_vpc_id}"
  vpc_id      = "${aws_vpc.main.id}"
  auto_accept = true
  accepter {
    allow_remote_vpc_dns_resolution = true
  }
}

data "aws_vpc" "existing" {
  count = "${var.use_peering ? 1 : 0}"
  id    = "${var.existing_vpc_id}"
}

resource "aws_route" "peering" {
  count                     = "${var.use_peering ? 1 : 0}"
  route_table_id            = "${aws_route_table.private-rt.id}"
  destination_cidr_block    = "${data.aws_vpc.existing[0].cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.main[0].id}"
  depends_on                = ["aws_vpc_peering_connection.main"]
}

resource "aws_route" "peering-2-existing" {
  count                     = "${var.use_peering ? 1 : 0}"
  route_table_id            = "${data.aws_vpc.existing[0].main_route_table_id}"
  destination_cidr_block    = "${aws_vpc.main.cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.main[0].id}"
  depends_on                = ["aws_vpc_peering_connection.main"]
}

resource "aws_security_group_rule" "sdk-2-ose" {
  count             = "${var.use_peering ? 1 : 0}"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["${data.aws_vpc.existing[0].cidr_block}"]
  security_group_id = "${aws_security_group.ose-sg.id}"
}

resource "aws_security_group_rule" "sdk-2-ipa" {
  count             = "${var.use_peering ? 1 : 0}"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["${data.aws_vpc.existing[0].cidr_block}"]
  security_group_id = "${aws_security_group.ipa-sg.id}"
}

resource "aws_security_group_rule" "sdk-2-artifactory" {
  count             = "${var.use_peering ? 1 : 0}"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["${data.aws_vpc.existing[0].cidr_block}"]
  security_group_id = "${aws_security_group.artifactory-sg.id}"
}