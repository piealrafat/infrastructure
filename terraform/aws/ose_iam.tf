resource "aws_iam_role" "ose_master_ec2_access_role" {
  name = "ose-master-access-role"
  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "ec2.amazonaws.com"
    },
    "Effect": "Allow",
    "Sid": ""
  }
]
}
EOF
}

resource "aws_iam_role" "ose_node_ec2_access_role" {
  name = "ose-node-access-role"
  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "ec2.amazonaws.com"
    },
    "Effect": "Allow",
    "Sid": ""
  }
]
}
EOF
}

resource "aws_iam_policy" "ose_master_policy" {
  name        = "ose-master-policy"
  description = "AWS Policy for OSE Masters"
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:DescribeVolume*",
                "ec2:CreateVolume",
                "ec2:CreateTags",
                "ec2:DescribeInstances",
                "ec2:AttachVolume",
                "ec2:DetachVolume",
                "ec2:DeleteVolume",
                "ec2:DescribeSubnets",
                "ec2:CreateSecurityGroup",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeRouteTables",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:RevokeSecurityGroupIngress",
                "elasticloadbalancing:DescribeTags",
                "elasticloadbalancing:CreateLoadBalancerListeners",
                "elasticloadbalancing:ConfigureHealthCheck",
                "elasticloadbalancing:DeleteLoadBalancerListeners",
                "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                "elasticloadbalancing:DescribeLoadBalancers",
                "elasticloadbalancing:CreateLoadBalancer",
                "elasticloadbalancing:DeleteLoadBalancer",
                "elasticloadbalancing:ModifyLoadBalancerAttributes",
                "elasticloadbalancing:DescribeLoadBalancerAttributes"
            ],
            "Resource": "*",
            "Effect": "Allow",
            "Sid": "1"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "ose_node_policy" {
  name        = "ose-node-policy"
  description = "AWS Policy for OSE Nodes"
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "ec2:DescribeInstances"
            ],
            "Resource": "*",
            "Effect": "Allow",
            "Sid": "1"
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "ose_master_policy_attach" {
  name       = "ose-master-policy-attachment"
  roles      = ["${aws_iam_role.ose_master_ec2_access_role.name}"]
  policy_arn = "${aws_iam_policy.ose_master_policy.arn}"
}

resource "aws_iam_policy_attachment" "ose_node_policy_attach" {
  name       = "ose-node-policy-attachment"
  roles      = ["${aws_iam_role.ose_node_ec2_access_role.name}"]
  policy_arn = "${aws_iam_policy.ose_node_policy.arn}"
}

resource "aws_iam_instance_profile" "ose_master_profile" {
  name  = "ose-master-profile"
  role = "${aws_iam_role.ose_master_ec2_access_role.name}"
}

resource "aws_iam_instance_profile" "ose_node_profile" {
  name  = "ose-node-profile"
  role = "${aws_iam_role.ose_node_ec2_access_role.name}"
}
