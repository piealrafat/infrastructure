#vpc
resource "aws_vpc" "main" {
  cidr_block = "${var.vpc_cidr_block}"
  tags = {
      Name = "vpc-${var.project_name}",
      Project = "${var.project_name}"
  }
}

#igw
resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
  tags = {
    Name ="igw-${var.project_name}",
    Project = "${var.project_name}"
  }
}


#natgw
resource "aws_eip" "nat-eip" {
  vpc = true
}

resource "aws_nat_gateway" "natgw" {
  allocation_id = "${aws_eip.nat-eip.id}"
  subnet_id = "${aws_subnet.public-1.id}"
  tags = {
    Name ="natgw-${var.project_name}",
    Project = "${var.project_name}"
  }
}

#key pairs
resource "aws_key_pair" "default" {
  key_name = "default"
  public_key = "${file("../../keys/id_rsa.pub")}"
}


#project vars to pass to ansible
resource "ansible_group" "all" {
  inventory_group_name = "all"
  vars = {
    project_name = "${var.project_name}"
    project_fqdn = "${var.project_fqdn}"
    ose_master_elb = "${aws_elb.ose_master_elb.dns_name}"
    ose_router_elb = "${aws_elb.ose_router_elb.dns_name}"
  }
}