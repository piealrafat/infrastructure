data "template_file" "ansible-ssh" {
  template = "${file("../templates/ssh.cfg.tmpl")}"
#  vars = {
#    bastion_ip            = "${aws_eip.bastion-eip.public_ip}"
#  }
}

resource "null_resource" "ansible-ssh-output" {
  triggers = {
    template = "${data.template_file.ansible-ssh.rendered}"
  }

  provisioner "local-exec" {
    command = "echo \"${data.template_file.ansible-ssh.rendered}\" > ../../ssh.cfg"
  }

  provisioner "local-exec" {
    command = "rm ../../ssh.cfg"
    when    = "destroy"
  }
}
