
resource "ansible_host" "ose-master" {
  count = "${var.ose_master_count}"
  inventory_hostname = "ose-master-${count.index}"
  vars = {
    ansible_ssh_host = "${element(aws_instance.ose-master-host.*.private_ip, count.index)}"
    openshift_node_group_name = "node-config-master-infra"
  }
  groups = ["masters","etcd","nodes"]
}


resource "ansible_host" "ose-node" {
  count = "${var.ose_node_count}"
  inventory_hostname = "ose-node-${count.index}"
  vars = {
    ansible_ssh_host = "${element(aws_instance.ose-node-host.*.private_ip, count.index)}"
    openshift_node_group_name = "node-config-compute"
  }
  groups = ["nodes","glusterfs"]
}

resource "ansible_group" "ose" {
  inventory_group_name = "OSEv3"
  children = ["masters", "nodes", "etcd", "glusterfs"]
}
