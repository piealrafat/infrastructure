resource "aws_elb" "ose_master_elb" {
  name = "ose-master-elb"
  security_groups = [
    "${aws_security_group.ose-sg.id}",
    "${aws_security_group.ose-master-sg.id}"
  ]
  subnets = [
    "${aws_subnet.services-1.id}",
    "${aws_subnet.services-2.id}",
    "${aws_subnet.services-3.id}"
  ]
  instances = "${aws_instance.ose-master-host.*.id}"
  internal = true
  listener {
    instance_port     = 8443
    instance_protocol = "TCP"
    lb_port           = 8443
    lb_protocol       = "TCP"
  }
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    target = "HTTPS:8443/"
    interval = 30
    timeout = 3
  }
  tags = {
    Name ="OSE Master ELB"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
}

resource "aws_elb" "ose_router_elb" {
  name = "ose-router-elb"
  security_groups = [
    "${aws_security_group.ose-sg.id}",
    "${aws_security_group.ose-infra-sg.id}"
  ]
  subnets = [
    "${aws_subnet.services-1.id}",
    "${aws_subnet.services-2.id}",
    "${aws_subnet.services-3.id}"
  ]
  instances = "${aws_instance.ose-master-host.*.id}"
  internal = true
  listener {
    instance_port     = 80
    instance_protocol = "TCP"
    lb_port           = 80
    lb_protocol       = "TCP"
  }
  listener {
    instance_port     = 443
    instance_protocol = "TCP"
    lb_port           = 443
    lb_protocol       = "TCP"
  }
  tags = {
    Name ="OSE Router ELB"
    Project = "${var.project_name}"
    "kubernetes.io/cluster/ose-${var.project_name}-cluster" = "owned"
  }
}