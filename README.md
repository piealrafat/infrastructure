# Combat Management Systems OTA1 Prototype

This repository contains the code and scripts necessary to stand up and maintain the
DevSecOps environment for the CMS OTA1 Prototype.

## System Requirements

* Docker: The code in this repository is configured to run inside of the docker container
defined in the build/ directory. As such, the only system requirement for running code in
this repository is having Docker installed and configured. Most recent versions of Docker
should work.

## Cloud Requirements

* AWS Gov Cloud: Currently the code in this repository is built around the AWS Gov Cloud
environment. The default settings use the us-gov-east-1 region. The IAM profile should
have read write privileges for S3, EC2, EBS, ELB, and VPC.
* AWS Provider Endpoints: The default AWS provider endpoints use AWS GovCloud FIPS
endpoints for security and accreditation. These can/should be overridden in non default
environments such as IL5 accounts with no Internet access that use custom endpoints, or
AWS commercial accounts.
* S3 Bucket: Terraform uses an S3 bucket backend for storing the state file. It is
important to not manage this bucket with the same terraform code as the rest of the system
so it cannot be accidentally deleted.
* AMI: The default AMI is a custom built Red Hat AMI for testing. This can be replicated
by building your own AMI, uploading to AWS, and setting the ami vars appropriately in the
`terraform/aws/vars.tf` file. Alternatively, the ami can be set to the standard Red Hat
image AMI.
* Red Hat: The default profile is to use a Bring Your Own License setup with Red Hat.
Access credentials, and org-ids are necessary for Red Hat and Openshift installs. If
using AWS Red Hat products start by changing the `rh_byol` variable to false in the
`ansible/inventory/group_vars/all/vars.yml` file.

## Quick Start

1. Create the Build Environment

```sh
docker build -t cms:latest .
```

2. Verify your Environment

```sh
# there are a lot of variables to make an alias
alias cms='docker run --rm -it \
  -v ${PWD}:/app \
  -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} \
  -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} \
  -e AWS_DEFAULT_REGION="us-gov-east-1" \
  -e VAULT_PASSWORD=${VAULT_PASSWORD} \
  cms:latest'
```

In areas where permissions on the volume are challenging, you may need to specify the
ansible.cfg location to avoid error regarding /app being world-readable.
```sh
  -e ANSIBLE_CONFIG=/app/ansible.cfg
```

For PowerShell users, there is a script cms.ps1 that can be run to setup your alias. It
will also prompt your for variables relevant to the alias.

```sh
# run the help output
cms -h
```

3. Init the repo

```sh
cms init
```

4. Provision the environment

```sh
cms provision
```

5. Install the environment

```sh
cms install
```

## Installing Openshift Components

Currently all openshift components need to be installed manually after running the steps
listed above in the Quick Start section. Openshift components are listed in the
`openshift/` directory and can be installed by running the following command:
`cms deploy <component>`

## Navigating this repository

### build

The `build/` folder contains resources to create the build environment. While the CMS
Prototype DevSecOps Environment scripts can be run from any machine with the necessary
pre-requisites, it is easier to build a docker container that has all necessary binaries
and files. The Dockerfile will build an environment that can run all of the terraform and
ansible scripts in a consistent environment between developers. The docker container
currently needs to have the code repository mounted to `/app` as well as environment
variables set for the ansible vault password, and AWS access keys and secrets.

### utils

the `utils/` folder contains helper scripts for running the code in this repository. The
`sdk-cli.sh` script is a top level user interface for interacting with the environment.
It contains helpful functions for provisioning, installing, and destroying the
environment. See `./utils/sdk-cli.sh -h` for more info.

### terraform

the `terraform/` folder contains all the terraform code for provisioning the environment.
Currently only AWS Gov Cloud us-gov-east-1 is supported. Future backends can be developed
using the necessary terraform plugins and resources.

Terraform currently uses the 
[Terraform Provider for Ansible](https://github.com/nbering/terraform-provider-ansible/)
to automatically generate an ansible inventory file for use with the configuration and
installation of the provisioned resources. See the documentation for how to add ansible
hosts and groups through terraform.

### ansible

the `ansible` folder contains all of the ansible playbooks and roles to install the
environment.

### openshift

the `openshift` folder contains all of the ansible playbooks and helm charts to deploy
applications into the openshift environment
